﻿var template = {}

template.portrait = function () {
    $("#canvasContainer").removeClass("paysage");
    $(".menu").removeClass("paysage");
    $("ul.menu li").addClass("paysage");  //Cette barre devient horizontale.
    $(".menu div").addClass("paysage");
}

template.paysage = function () {
    $("#canvasContainer").addClass("paysage");
    $(".menu").addClass("paysage");
    $("ul.menu li").removeClass("paysage");
    $(".menu div").removeClass("paysage");
}

/*Construit les barres d'outil*/
template.compile = function () {
    var buttons = $(".geobuttonbar input[type=button]");
    var result = [];
    for (var nd = 0; nd < buttons.length; ++nd) {
        var button = buttons.eq(nd);
        result.push({ _title: button.attr("title"), _class: button.attr("class"), _onclick: button.attr("onclick") });
    }
    return JSON.stringify(result);
}

template.initMenu=function(pathImage) {   
    prim.initPattern(pathImage + 'hachure.jpg');
    var body = $("#canvasContainer").parent();
    if (body.width() < body.height())
        template.portrait();
    else template.paysage();
    this.geo = { plane: plane, figure: figure, control: geocontrol }

    /*Utile si usage du ul...*/
    $("ul.menu li").click(function () {
        plane.Selected(false);
        $(".submenu").css("display", "none");
        var ndex = $(this).index();
        console.log("click menu?" + ndex);
        $(".submenu").eq(ndex).css("display", "inline-block");
    });
}

/*Utile si on utilise div pour menu au lieu de ul li*/
template.showSubmenu = function (sub_name) {
    plane.Selected(false);
    $(".submenu").css("display", "none");
    $(sub_name).css("display","inline-block");
}

template.txtToHTML=function(txt){
	return txt.replace(/\s/g,'<br>');
}

/*Utile pour enterineDialog*/
template.inputField=false;

/*Affichage textarea ou bien boîte simple*/
template.prompt=function(big){
	var display=$("#prompt");
	var hidden=$("#bigprompt");
	if (big) {
	    hidden = display;
	    display = $("#bigprompt");
	}
	template.inputField=display;
	display.css("display","block");
	hidden.css("display","none");
	$("#dialog_geo").fadeIn();
    display.focus();
}

template.setUnit=function(){
	plane.repere.unitx = $("#unitx").val();
	plane.repere.unity = $("#unity").val();
	plane.updateScale();
	plane.redraw();
}

/*Pour passer des paramètres dans la chaîne de connexion
Pour passer consigne, ne pas mettre de paramètre appelé schema ou figure
     * schema: passer url image
     * figure: passer figure tactigeo (json)
     * Autre: passe consigne
     * @return json data(objets à charger ensuite)|
     * true(objet texte|fonction)|
     * false(pas d'objet)
     */
template.initFromUrl = function () {
    var params = getParamUrlAsObject();
    var msg = "";
    var file = false;
    var fparam;
    plane.removeAll();
    if (mode = params["mode"]) {//euclid|analytic
        plane.mode = mode;
    }
    if (img_url = params["schema"]) {//image
        plane.image = img_url;
    }
    if (data = params["figure"]) {
        file = JSON.parse(data);  //Données json, 
        return file;  //object
    }  
    if (fdex1 = params["fdex1"])  //Fonction
        file=figure.tryAddFunction(fdex1, '');
    for (var key in params) {  //Message
        if (key.length < 4)  //Bizarre...
            msg += decodeURI(params[key])+'\n';
    }
    if (msg.length) {
        plane.add(figure.htmlMessage(20, 20, msg));  
        file = true;
    }
    return file;
}

/**Initie tout*/
with (Tactileo.Quiz) {
    geomath();
    prim();
    figure();
    plane();
    geocontrol();
    geoloader();
    geosvg();
}

plane.pointButton = { _title: "Point", _class: "point", _onclick: "geocontrol.glue()" };

plane.undoButton = { _title: "Supprimer la dernière action", _class: "undo", _onclick: "geocontrol.undo()" };

plane.commonButtons = [plane.undoButton,
                       plane.pointButton,
                      { _title: "Entrer du texte", _class: "message", _onclick: "geocontrol.text()" }];

plane.stdbuttons =
    [
        //{ "_title": "Supprime un objet", "_class": "cut", "_onclick": "geocontrol.property('hidden')" },      
        { _title: "Milieu", _class: "midpoint", "_onclick": "geocontrol.pick('milieu')" },
        { _title: "Segment", _class: "segment", "_onclick": "geocontrol.slide('SEGMENT')" },
        { _title: "Droite", _class: "line", "_onclick": "geocontrol.slide('DROITE')" },
        { _title: "Cercle", _class: "circle", "_onclick": "geocontrol.slide('CERCLE')" },
        { _title: "Arc rapide", _class: "compas", "_onclick": "geocontrol.arc()" }
        ];
plane.advanced=[
        { _title: "Parallele", _class: "parallele", "_onclick": "new geocontrol.pick('para')" },
        { _title: "Perpendiculaire", _class: "perpendiculaire", "_onclick": "new geocontrol.pick('perp')" },
        { _title: "Polygone", _class: "polygon_small", "_onclick": "geocontrol.collection()" },
        { _title: "Arc de rayon fixe", _class: "egalite", "_onclick": "geocontrol.arc_ruled()" },
        { _title: "Angle entre 2 segments", _class: "angle", _onclick: "geocontrol.pick('angle')" },
        { _title: "Secteur circulaire", _class: "sector", "_onclick": "geocontrol.sector('sector')" },          
		{ _title: "Distance-2 points", _class: "distance", _onclick: "geocontrol.distance()"},
		{_title:"Symétrie centrale", _class: "symmetry", _onclick:"geocontrol.pickpoints('symecentre')"},
		{_title:"Symétrie axiale", _class:"line_symmetry", _onclick:"geocontrol.pickpoints('symeaxe')"}
    ];

plane.analytic = [{ _title: "Repère", _class: "repere", "_onclick": "geocontrol.invertRepereVisible()", bool: false },
                  { _title: "f(x)", _class: "fdex", "_onclick": "geocontrol.Function()" },
                  { _title: "ro(theta)", _class: "spirale", "_onclick": "geocontrol.Function('polaire')" },
                  { _title:"Horizontale", _class:"horiz", _onclick:"geocontrol.pick('horiz')"},
                  { _title: "Sinus", _class: "sine2", "_onclick": "geocontrol.slide('SINE')" }];
        
plane.figure_control = [{ _title: "Supprime tout", _class: "trashcan", "_onclick": "geocontrol.removeAll()",bool:false },
     { _title: "Animer", "_class": "pellicule", _onclick: "plane.animate()",bool:false },
     { _title: "Translater la figure", _class: "translate_cursor", "_onclick": "geocontrol.translate()",bool:false },
     { _title: "Verrouiller la scène", _class: "cadenas", "_onclick": "plane.lockAll()",bool:false},
     { _title: "Grille magnétique", _class: "grille", "_onclick": "plane.redrawAfter('gridset')", bool: true,field:plane.gridset },
     { _title: "Nommer automatiquement les points", _class: "AB", _onclick: "plane.redrawAfter('pointAutoName')", bool: true, field: plane.pointAutoName },     	
     { _title: "zoomx2",_class: "loupe",_onclick: "geocontrol.zoom()", bool:false}
];

plane.figure_prop = {
    pointille: "dashed",
    mesure: "distance",
    annote: "text",
    edit_text: "crayon_gris",
    coord: "coordonnee",
    fill: "remplir",
    hachure:"carreau",
};  //cf propriétés figures (nom methode, css)




/*Usage du tableau compilé:
<div data-bind='foreach: geobuttons'>
 * <input type=button data-bind="attr:{title:_title},css:_class,click:function(){eval(_onclick)}"/>
 * </div>
 */
 
 /*Ajout bouton:
 Avec outil buttonbar.htm:
 1)Rajouter image dans le tableau au début puis exécuter
 2)Créer le canvas avec les images puis sauvegarder l'image
 3)Créer le css qu'on mettra dans geoStyle.css
 4)Modifier template_compiler.js pour lier bouton (classe) et action*/