﻿/**Au démarrage de l'appli: dédié à Tactileo Quiz (pas la version de test)*/
Tactileo.Quiz.PrepareGeo = function (editor, actif) {

    editor.geoVersion = 1.80;
    console.log("is preparing geo");

    /**actif signifie la présence du geomenu et geobuttonbar*/
    if (actif) $.get("/Modules/Tactileo.Quiz/Scripts/geometry/template.htm",
        function (data) {
            $("body").append(data);
            editor.plane = plane;
            if (editor.terminate) editor.terminate();
        });  //Charge le template, encore caché=>binding le chargera effectivement

    /**Au démarrage de la question*/
    editor.initGeo = function (serviceData) {
        $('#rightgeomenu').append($('.geobuttonbar'));  /*Remplace right_menu.click,voir template*/
        plane.initCanvas(100);

        prim.initPattern("/Modules/Tactileo.Quiz/Styles/images/carreau.jpg");
        plane.removeAll();   //Colle un repère au plan. 

        if (serviceData) {
            plane.intelliLoad(serviceData.Figure);
            //serviceData.DoNotEvaluate = true;
        }

        if (plane.canvas) plane.redraw();
    }

    Tactileo.Quiz.Geo = {
        NAME: 'Geo', save: function () {
            var scene = plane.intelliSave();
            var liste = JSON.parse(scene.liste);

            return { Figure: liste, DoNotEvaluate: true };
        }
    };

    editor.specializeGeo = function () {  //Utile pour binding editeur
        if (editor.specializedQuestion) editor.specializedQuestion(Tactileo.Quiz.Geo);
    }

}