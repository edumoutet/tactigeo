﻿Tactileo.Quiz.geoloader=function () {

    //NB: a,b: clefs référencant un objet, creator spéciale
    //name,text:string
    //x,y:int
    //hidden: bool
    //color:#xxxxxx
    //anno: [mesure,annote] segment,angle
    //locked: bool
    //action: methode liaison polygône
    //fill: bool-figure pleine(polygône, secteur)
    //expr: pour fonction
    //coord: coordonnées d'un point
    //type: pour fonction (type=false|pôlaire)
    var isProperty = { 'text': 1, 'color': 1, 'anno': 1, 'orient': 1, 'rayon': 1, 'action': 1, 'expr': 1, 'pas': 1, 'fill': 1, 'coord':1, 'type':1, unitx:1, unity:1 }
   
    //Méthode pour tester save/reload
    plane.saveReload = function () {
        var objs = JSON.parse(plane.intelliSave());
        console.log("saved:");
        console.log(objs);
        var reload = plane.intelliLoad(objs);
        console.log("reload:");
        console.log(reload);
        return reload;   //todo: plane.objets(reload);
    }

    plane.intelliSave = function () {
        /*La réaffectation rend certains objets redondants, ils sont filtrés à la sauvegarde*/
        var objets = plane.objets().filter(function (obj) { return ("dontsave" != obj.creator); });

        /**compacter va exprimer les références sous forme d'index au lieu de l'objet complet**/
        function compacter(obj) {
            var result = {};
            if (obj.name == "Message") return figure.saveMessage(obj.ref);
            if (math.isPoint(obj)) result.point = { x: obj.x, y: obj.y, locked: obj.locked }//sinon: result.point=null
            if (obj.a || obj.link) result.link = [];

            for (var key in obj) {
                if (typeof (obj[key]) == 'function') continue;
                if (!obj[key]) continue;

                switch (key) {
                    case 'name': result.name = obj.name; break;
                    case 'hidden': result.hidden = obj.hidden; break;
                    case 'a': if (!obj.creator) result.link.push(reference(obj.a)); break;  //NB: obj.creator gère a et b, ne pas les sauvegarder
                    case 'b': if (!obj.creator) result.link.push(reference(obj.b)); break;
					case 'helper': if (!obj.creator) result.link.push(reference(obj.helper)); break;
                    case 'creator': result.creator = compacter(obj.creator); break;
                    case 'link': for (var k = 0; k < obj.link.length; ++k) result.link.push(reference(obj.link[k]));  //Objets de type collection. Au moins 3 liens
                    default:
                        if (!result.property) { result.property = []; }
                        if (isProperty[key]) {
                            result.property.push({ key: key, value: obj[key] });
                        }
                        //if (!result.property) result.property = {};
                        //if (isProperty[key]) result.property[key] = obj[key];
                }
            }
            return result;
        }

        //Satellite intelliSave
        function reference(obj) {//console.log("reference:");
            //console.log(obj);
            var ndex = objets.indexOf(obj);
            if (ndex >= 0) return ndex;   //Cas usuel: on prend un entier
            else if (typeof (obj) == 'string') return obj;     //Ex: self
            else return obj;   //Ni référencé, ni dans objets du modèle (Ne doit pas arriver)
        }

        var liste = [];
        for (ndex in objets) {
            var obj = objets[ndex];
            var result = compacter(obj);
            liste.push(result);  //Ex:{a:pt0,b:obj1}
        }
        //return JSON.stringify({objets: liste, property: property, hidden: hidden});
        
        return { "liste": JSON.stringify(liste), 
                  "title": plane.title(), 
                  "figure_option": JSON.stringify({repere:!plane.repere.hidden,AB:plane.pointAutoName(),grille:plane.gridset(), image:plane.image()}), 
                  "_id": plane._id 
                  }
    }

    /*liste: objet JSON issu de intelliSave.{objets[],creator{},hidden[],property{}}
      option: propriétés de la figure: repère visible, grille, points nommés
    */
    plane.intelliLoad = function (liste,option) {
	    console.log("intelliload");
	    console.log(liste);
	    console.log(option);
        var objs = [];

        function getRef(ndex) {
            if (/\d+/.test(ndex)) return objs[ndex];  //chiffres uniquement
        }

        function decompacte_lien(obj,dest) {
            var result = [];
            var link = obj.link;
            if (obj.name!='COLLECTION') {//L'objet n'est pas de type collection: segment, cercle... link.length < 3	   
                dest.a = getRef(link[0]);
                if (link.length >= 2)
                    dest.b = getRef(link[1]);		
				if(link.length==3) dest.helper=getRef(link[2]);
            }
            else {
                link.forEach(function (item) { result.push(getRef(item)); });  //int=>objet
                dest.link = result;
            }
        }

        function decompacte_param(obj,dest) {
            if (obj.link) decompacte_lien(obj,dest);
            else {
                dest.a = getRef(obj.a);
                dest.b = getRef(obj.b);
            }
        }

        /**Transforme obj.property en dico!*/
        function loadProperty(obj, dest) {
            var result = {}
            if (obj.property && obj.property.length)
                obj.property.forEach(function (item) { result[item.key] = item.value; });
            dest.property = result;
        }

        liste.forEach(function (obj) {
            var result = {};      
            if ("dontload" == obj.name) return;  //pour point d'intersection dépendant de son 'frère'

            var clone = { point: obj.point };  //Accueille property, link, a,b. objet intermédiaire, évite de modifier obj = objet brut 
            loadProperty(obj,clone);  //[{key,value}]=>dico car loader peut nécessiter une version dico des propriétés
            decompacte_param(obj, clone);    //Affecte a,b,link

            //console.log("clone=");
            //console.log(clone);
            var creator = obj.creator;
            var _creator = {}
            if (creator) {//Objet a un délégué (creator)-intersection, droite parallèle...
                decompacte_param(creator, _creator);  //Affecte a,b,link     
                result = figure.creeObjet(creator.name, _creator.a, _creator.b);
            }
            else if (meth = figure.loader[obj.name])  //Cas particulier: texte, repere, fonction ...
                result = meth(clone);
            else if (obj.name) {  //Cas standard
                result = figure.creeObjet(obj.name, clone.a, clone.b);  //Habituel-segment, cercle  
            }
            if (!result) return;  //Signifie qu'on a fini les traitements.

            /**Complétion*/
            for (var key in clone.property)
            { result[key] = clone.property[key]; }

            if (p = obj.point) { result.x = p.x; result.y = p.y; result.locked = p.locked; }        

            result.hidden = obj.hidden;

            //console.log("result="+obj.name);
            //console.log(result);

            objs.push(result);
            if (result.brother) objs.push(result.brother);

        });
        plane.objets(objs);
        if(option){
          plane.repere.hidden=!option["repere"];
          plane.gridset(option["grille"]);
          plane.pointAutoName(option["AB"]);
          plane.image(option["image"]);
        }
        geocontrol.undoStack = [objs.length];
    }
	
	plane.load=function(geofigure){
		if(typeof(geofigure)=='string')   //Réception serveur.
		  geofigure=JSON.parse(geofigure);
	    console.log(geofigure);
	    if(typeof(geofigure.liste)!='undefined')
	        plane.intelliLoad(geofigure.liste,geofigure.option);
	    else plane.intelliLoad(geofigure, false);  //Ancien modèle
	    plane.updateScale();
	    plane.redraw();
	}
	plane.save=plane.intelliSave;

	 /**File management
    plane.toFile = function (dir,filename) {  //Ajax save!
        if (!filename) { alert("Renseigner un nom!"); return; }
        var envoi = plane.intelliSave();
		envoi.dir = dir;
        envoi.filename = filename;
        envoi.goal = 'save'; //{ liste, filename, goal }
        $.post("geoloader.php", envoi).done(function (liste) { manager.log.value = liste; });
    };
	plane.fromFile = function (dir,filename) {
        var envoi = { goal: "load", filename: filename, dir: dir };
        $.post("geoloader.php", envoi).done(
            function (recep) {
                recep = JSON.parse(recep);
                plane.intelliLoad(recep);
                plane.redraw();
            });
    }
	**/

    plane.toSVG = function (service, filename, elem) {
        var envoi = { svg: plane.saveSVG() };
        if (elem) { $(elem).html(envoi.svg); return; }
        if (!filename) { alert("Renseigner un nom!"); return; }
        envoi.filename = filename;
        envoi.goal = 'svg';
        $.post(service, envoi).done(function (recep) { manager.log.value = recep; });
    }
	
	plane.toHTML=function(service,filename){
		var envoi={liste: plane.save().liste, goal:'htm', filename:filename};
		$.post(service,envoi).done(function(recep){manager.log.value=recep;});
	}

    plane.loadFromString = function (str) {
        if (!str || !str.length) return;
        var vm = JSON.parse(str);
        console.log(vm);

        var objs = plane.intelliLoad(vm);
        console.log(objs);

        plane.objets(objs);
        return true;
    }
   
};

