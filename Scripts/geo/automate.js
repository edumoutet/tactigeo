﻿/*Création de figures par script*/
var automate_geo = function (centr, wid, hei) {

    function randomInt(ampli) {
        return Math.round(Math.random(ampli)) - ampli / 2;
    }

    function randomPoint() {
        return { x: centr.x+randomInt(wid), y: centr.y+randomInt(hei) };
    }

    function randomPtOnCircle(circle) {
        var ang = Math.random(2 * Math.PI);
        var pt = math.polaire(ang, circle.getRayon());
        return { x: circle.a.x + pt.x, y: circle.a.y + pt.y };
    }

    /*Triangle de dimensions fixées mais positionnement aléatoire*/
    function triangle(ab, ac, bc) {
        var pt = randomPoint();
        var cer = figure.creeObjet("CERCLE",pt, ab);
        var pt2 = randomPtOnCircle(cer);
        var cer2 = figure.creeObjet("CERCLE",pt, ac);
        var cer3 = figure.creeObjet("CERCLE",pt2, bc);
        var binom = math.interCircle(cer2, cer3);
        plane.add(pt);
        plane.add(cer, true);//true signifie caché...
        plane.add(pt2);
        plane.add(cer2, true);
        plane.add(cer3, true);
        plane.add(binom[0]);
        plane.add(figure.COLLECTION("rely", [pt,pt2,binom[0]]));
        plane.redraw();
    }

    /*params: centre, point, angles en radian*/
    function wheel(cen, pt, angs) {
        plane.add(cen);
        plane.add(pt);
        var cer = plane.addObject("CERCLE", cen, pt);
        var ndex = 0;
        var colors = prim.colors();
        angs.forEach(function (ang) {
            var pt2 = math.imageRota(cen, ang, pt);
            var glued=plane.addGlueOn(pt2, cer);
            plane.addObject("SEGMENT", cen, glued);
            var sec = plane.addObject("SECTOR", {a:cer.a, b:pt}, glued);
            sec.fill = colors[ndex++];
            pt = glued;
        });
    }

    function exemWheel() {
        var x = -Math.PI/12;
        wheel({ x: 200, y: 200 }, { x: 400, y: 200 }, [3*x, x, 2*x]);
        plane.redraw();
    }

    return { triangle: triangle, wheel: wheel, exemWheel: exemWheel };
}