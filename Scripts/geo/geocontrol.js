﻿/**Helper pour construction interactive des objets**/
Tactileo.Quiz.geocontrol = function () {
    Intent = false;
   
    plane.intent = function () { return Intent; };
    geocontrol = { undoStack: [1] };   //Pour annuler une action: partir de 1 car le repère a pour indice 0
	
	 /**override plane.defaultAction**/
    //plane.defaultAction = geocontrol.tryIntersect;

    var vecteur = math.vecteur;
    var writeMessage = prim.writeMessage;
    var kindOf = figure.kindOf;
    var isPoint = math.isPoint;
	var addObject=plane.addObject;

    /**Gabarit**/
    var PT = { x: 0, y: 0, name: false };
    var PTEXIST = { x: 0, y: 0, name: false };    //Point doit exister
    var SEG = { name: "SEGMENT", a: PT, b: PT, message: "Segment" };
    var CER = { name: "CERCLE", a: PT, b: PT, message: "Cercle donné par centre et un point" };
    var GAB = {
        pt: PT,
        seg: SEG,
        cercle: CER,
        para: { name: "PARA", a: SEG, b: PT, message: "Parallèle à un segment", finish: "Tapez un point" },
        perp: { name: "PERP", a: SEG, b: PT, message: "Perpendiculaire à un segment", finish: "Tapez un point" },
        sector: { name: "SECTOR", a: CER, b: PT, message: "Secteur circulaire: tapez un point", finish: "Fini!" },
        rapporteur: { name: "RAPPORTEUR", a: CER, b: PT, message: "Rapporteur: tapez un point", finish: "Fini!" },
        angle: { name: "ANGLE", a: SEG, b: SEG, message: "Angle entre deux segments", finish: "Tapez un segment ou une droite" },
        droite: { name: "DROITE", a: PT, b: PT, message: "Droite passant par deux points", finish: "Tapez un point" },
        sine: { name: "SINE", a: PT, b: PT, message: "Sinusoide passant par deux points", finish: "Tapez un point" },
        arc: { name: "ARC", a: PT, b: PT, message: "Arc de compas", finish: "Tapez la zone de l'arc" },
        distance: { name: "DIST", a: PTEXIST, b: PTEXIST, message: "Distance entre deux points", finish: "Tapez un second point" },
        milieu: { name: "MILIEU", a: SEG, b: false, message: "Milieu d'un segment" },
		symecentre:{name:"SYMECENTRE", a:PTEXIST, b:PTEXIST, message:"Symétrique d'un point par rapport à un autre", finish: "Entrez le centre"},
		symeaxe: { name: "SYMEAXE", a: PTEXIST, b: SEG, message: "Symétrique d'un point par rapport à un axe", finish: "Cliquez l'axe" },
        horiz:{name:"Horiz", a:PT, b:false, message:"Droite Horizontale variable"}
    };

    function explicitGabs() {
        var result = {};
        for (var key in GAB)
            if (name = GAB[key].name) result[name] = GAB[key].message;
        return result;
    }
    var messages = explicitGabs();

    /**Pendant en fait**/
    function hasChanged(s, gab, field) {
        plane.pdown = false;
        var _name = gab[field].name;
        if (!_name) _name = "point";
        plane.description(s ? gab.finish : 'Mauvaise manip: ' + _name + ' attendu', 30, 30);
    }

    /**Action a réussi**/
    function hasEnded(releasepdown, message) {
        with (plane) {
            description(message);
			hasRule(false);
            if (!Intent.isProperty) geocontrol.registerAction();  //Prépare undo pour création
            Intent = false;
            redraw(keepSelected = true);
            if (releasepdown) pdown = false;
        }
    }

    /**todo: manage property actions (color...)*/
    geocontrol.undo = function () {
        var undostack = geocontrol.undoStack;
        if (undostack.length <= 1) return;   //bordure
        with (plane) {
            var nd1 = undostack.pop(), nd2 = undostack.pop();  //Dépile les deux points d'arrêt les plus récents!
            for (var nd = nd2; nd < nd1; ++nd) {
                objets.pop();
            }
            Intent = false;
            redraw();
            undostack.push(nd2);   //Besoin de remettre indice le plus ancien.
            //console.log("Dépile:"+nd2);
        }
    }

    /**Avec deux points. rayon=objet arc pour fixer le rayon***/
    geocontrol.slide = function (name, rayon) {
        Intent = { name: name, a: false }
        plane.creation(messages[name] + " Renseigner deux points!");
        Intent.change = function (pt) {
            with (plane) {
                var exist = selecPoint(pt);
                var firstP = (exist ? exist : pt);
                if (!exist) firstP = addGlue(firstP);  
                pdown = { x: firstP.x, y: firstP.y }  //Clonage...
				 
                if (rayon) {//arc_egal		   
                    var seg = figure.creeObjet("SEGMENT", firstP, pdown);
                    Intent.obj = figure.creeObjet(name, rayon, seg);
                }else Intent.obj = figure.creeObjet(name, firstP, pdown);
                if(plane.hasRule()) 
					Intent.obj.setRayon(plane.rule());		
                console.log("geocontrol slide:");
                console.log(Intent.obj);				
                Selected(Intent.obj);  //Pour dessin
            }
        }
        Intent.mouseup = function (silent) {//silent si Intent a un objectif intermédiaire.
            with (plane) {
                var current = Intent.obj;
                if (math.egalite(current.a, pdown)) {  //On a cliqué seulement
                    if (current.setRayon) {
                        geocontrol.waitSecondPoint(current);   //slide devient pick2!
                        pdown = false;
                        return;
                    }
                    else pdown.x = current.a.x + 4 * math.EPS;  //Différencie les deux points!
                }  
                var exist = selecPoint(pdown);
                if (exist) {  //Il faut abandonner pdown comme point de l'objet puisque un point existe déjà
                    current = figure.creeObjet(name, current.a, exist);  //Remplacement obligatoire (variables final...)
                    Selected(current);
                }
                else
                    add(pdown);
                add(current);
				plane.rule(math.distance(current.a,pdown));
                if (silent) { pdown.hidden = true; pdown = false; }
                else hasEnded(true);
				
                return current;
            }
        }
    }

	/*pick x2 pour (point|objet) existants*/
	geocontrol.pickpoints = function(gabkey){
	  var gab=GAB[gabkey]; 	
	  Intent = {name: gab.name, search:kindOf(gab.a)}
	  plane.description(gab.message);
	  Intent.change = function(pt){
	     plane.mouseup = true;  //pour provoquer mousedown 
		 plane.pdown = false;
	     var exist = plane.selecGeneric(pt, Intent.search);
	     if (!exist) return;  //Echec.
	     prim.selecShow(exist);
		 Intent.search=kindOf(gab.b);  //Si undefined, on va chercher un point en deuxième pick.
	     if (!Intent.a) {
	         Intent.a = exist;
	         plane.description(gab.finish);
	     }
	     else {
	         plane.addObject(gab.name, Intent.a, exist);
	         hasEnded();
	     }
	  }
	  Intent.mouseup = function () {
	  
	  }
	}

    /*redirige un control slide dont l'objet a setRayon; */
	geocontrol.waitSecondPoint = function (obj) {
	    plane.hasRule(true);
	    plane.ruleHelp(obj.a);
	    Intent.change = function (pt) {
	        plane.add(pt, true);
	        obj = plane.addObject(obj.name, obj.a, pt);
	        obj.setRayon(plane.rule());
	        hasEnded();
	    }
	    Intent.mouseup = function () { }
	}
	
	geocontrol.distance=function(){geocontrol.pickpoints("distance");}

    /**Générique pour objet(existant) + point(existant ou non)**/
    geocontrol.pick = function (gabkey) {
        var gab = GAB[gabkey];

        Intent = { name: gab.name };

        plane.creation(gab.message);
        plane.mouseup = true;  //pour provoquer mousedown 

        Intent.change = function (pt) {  //Renseigner obj.a et obj.b!
            console.log("Intent change!" + gab.a.name);
            var valid = true;
            with (plane) {
                if (!Intent.a) {
                    if (gab.a == PT) {  
						var exist = selecPoint(pt);
						if(!exist) addPoint(pt);
						Intent.a = exist?exist:pt;
						if(plane.hasRule()) plane.ruleHelp(Intent.a);  //Rayon fixé! et affiché
					}
                    else Intent.a = selecObjet(pt,kindOf(gab.a));
                    hasChanged(Intent.a, gab, 'a');
                    mouseup = true;
                    if (!Intent.a) return;
                    else prim.selecShow(Intent.a, gab.message, prim.BLUE);
                    if (!gab.b) { addObject(gab.name, Intent.a); hasEnded(); }  //Un seul objet requis.
                    return;
                }
                else {//Si deuxième objet requis
                    if (gab.b == PT) {
						var exist = selecPoint(pt);
                        if (!exist) addPoint(pt);
						else pt = exist;
                        var obj = addObject(gab.name, Intent.a, pt);
						if(obj.setRayon) obj.setRayon(plane.rule());
                        hasEnded();
                    } else if (obj = selecObjet(pt,kindOf(gab.b))) {
                        console.log("End intent!");
                        addObject(gab.name, Intent.a, obj);
                        hasEnded();
                    } else hasChanged(false, gab, 'b');
                }
            }
        }

        Intent.mouseup = function () {  
            
        }

    }

    /**Param suppl?mentaire pour arc!**/
    function sector(orient) {
        new geocontrol.pick('sector');
        /*override*/
        Intent.parent_change = Intent.change;
        Intent.change = function (pt) {
            with (plane) {
                if (!Intent.a)
                    if (c = selecObjet(pt,"circle")) {
                        remove(c);
                        Intent.a = c;
                        Intent.message(c);
                        pdown = pt;  //Provoque redraw...
                        Intent.parent_change(pt);
                    }
            }
        }
    }

    /**Construit un secteur sans cercle pr?alable.***/
    geocontrol.sector = function (sectorname) {  //sector,rapporteur=name
        new geocontrol.slide('SEGMENT');
        plane.creation("Secteur angulaire: construisez d'abord un segment");
        Intent.parent_mouseup = Intent.mouseup;
        Intent.mouseup = function () {  //NB: fin du cercle. 
            Intent.parent_mouseup();  
			var seg = plane.Selected();  
			new geocontrol.pick(sectorname);
			plane.ruleHelp(seg.a);
            Intent.a = seg;
            //cer.hidden = true;
        }
    }

    geocontrol.arc = function () {
        geocontrol.slide("ARC");   //Constru classique avec deux points
    }
	
	geocontrol.arc_ruled_ = function(){
		geocontrol.slide("ARC");
		plane.regler();  //Va donner un rayon fixe à l'arc!
	}
	
	geocontrol.arc_ruled = function(){
		geocontrol.pick("arc");
		plane.regler();
	}

    /**Lancé si Selected.getRayon()*/
    geocontrol.arc_egal = function () {
        var arc = plane.Selected();
        if (!arc || !arc.getRayon) { geocontrol.arc(); return; }  //En l'absence de référence de rayon, on fait un arc classique!
        //if (arc.name != 'ARC') { hasEnded(true,'Erreur : no arc'); return false; }
        geocontrol.slide("ARC_EGAL", arc);
		
        plane.creation("Arc de rayon fixe");
        var parent_mouseup = Intent.mouseup;
        Intent.mouseup = function () {
            console.log("Intent obj");
            console.log(Intent.obj);
            var arc_egal = Intent.obj;
            var direction = arc_egal.b;  //point
            direction.hidden = true;
            direction.b.hidden = true;
            plane.addIfMisses(direction.a);
            plane.addIfMisses(direction.b);
            plane.add(direction);
            plane.add(arc_egal);
            plane.Selected(arc_egal);
            plane.pdown = false;
            plane.redraw(true);
            hasEnded(true);

            //geocontrol.arc_egal(arc);  //Relance
        }
    }

    /*Ex: intentProperty("annote")*/
    geocontrol.property = function (name) {
        Intent = { name: name, isProperty: true };
        plane.description("Propriété:" + name);
        plane.mouseup = true;
        Intent.change = function (pt) {
            with (plane) {
                var s = Selected();
                if (!s || s.locked) return;
                s[name] = true;
                pdown = false;
                Selected(false);
                hasEnded();
                redraw();
            }
        }
    }

    /**Etre générique: tente d'ajouter un point sur objet existant-devrait être la règle*/
    geocontrol.glue = function () {
        Intent = { name: 'Point' }
        plane.creation("Créer un point");
        var ptoadd;
        Intent.change = function (pt) {
            with (plane) {
                var exist = selecPoint(pt); 
				if(exist){ hasEnded(); return; }  //Aucune action
                var inter = geocontrol.tryIntersect(pt);
				console.log(inter);
                if (inter) { 
					hasEnded(true); return; 
					}
                pdown = pt;
                ptoadd = pt;
            }
        }
        Intent.mouseup = function () {
            plane.addGlue(ptoadd);
            plane.Selected(ptoadd);
            hasEnded(true);
        }
    }

    /*Crée un point - concur geocontrol.glue - dépréciée*/
    geocontrol.point = function (attached) {
        Intent = { name: "POINT" };
        plane.description(attached ? attached.name : "Créer un point");

        Intent.change = function (pt) {
            with (plane) {
                var s = Selected();
                if (!isPoint(s)) //Il ne faut pas avoir un point sélectionné! 
                {
                    if (!attached) addPoint(pt);
                    else {
                        plane.addAt(attached, pt);  //Objet attaché en un point
                    }
                    //else addObject(attached,false,false,pt);  
                }
            }
        }

        Intent.mouseup = function () {
            plane.pdown = false;
            hasEnded();
        }
    }

	/*Ajout div au-dessus du canvas-idée: mettre html*/
    geocontrol.text = function () {
        plane.creation("Cliquez pour entrer un texte");
        Intent = { name: "TEXT" }
        Intent.change = function (pt) {
            plane.pdown = false;
            writeMessage("[...]", pt.x, pt.y);
            //plane.description("");
            //var txt = figure.TEXT(prompt("Entrez le texte:"));
            //Après l'entrée: (callback dialogue)
            Intent.ends = function (text) {
                if (text.length) {           
                    var obj = figure.htmlMessage(pt.x, pt.y, text);
                    plane.add(obj);
                }
                hasEnded(true);
            }
            geocontrol.geoprompt({ helpText: "Entrez le texte" }, true); //param 2: big
        }
        Intent.mouseup = function () { }
    }

    /*typ='polaire' ou rien*/
    geocontrol.Function = function (typ) {
        plane.creation("Cliquez pour entrer une fonction");
        Intent = { name: "FUNCTION" };
        Intent.change = function (pt) {
            plane.pdown = false;
            prim.draw(pt, prim.LIGHTCOLOR);
            plane.description("Entrez f(x):");
            var helpText = "Expression de x";
            if (typ == "polaire") helpText = "Expression de l'angle x";
            //var exprX= prompt("Entrez f(x)");
            Intent.ends = function (expr) {
                figure.tryAddFunction(expr, typ);
                hasEnded(true);
            }
            geocontrol.geoprompt({helpText: helpText});
        }
        Intent.mouseup = function () { };
    }

    geocontrol.translate = function () {
        Intent = { name: "Translate" }
        Intent.change = function (pt) {
            var obj = { a: { x: pt.x, y: pt.y }, b: plane.pdown }
            obj.rely = function () {
                plane.translate(vecteur(obj.a, obj.b));
                obj.a = { x: obj.b.x, y: obj.b.y }
            }
            plane.Selected(obj);  //Entraine obj.rely donc effectue la translation
        }
        Intent.mouseup = function () { hasEnded(true); }
    }

    //plane.actions = ["rely", "fill", "curve"];  //Pour collection
    geocontrol.collection = function (action) {
        var tab = [];
        Intent = { name: "collection", tab: tab }
        plane.creation("Polygône: ajoutez des points");
        Intent.change = function (pt) {
            pt = plane.addIfMisses(pt);
            if (tab.indexOf(pt) == (-1)) {
                tab.push(pt); plane.pdown = false;
                plane.description("collection: ajout point:" + tab.length);
                plane.redraw(false,true);  //no selec, description
            }
            else {
                var poly = new figure.COLLECTION(action, tab);
                    
                plane.add(poly);
                //plane.addObject("COLLECTION", "fill", tab);
                hasEnded(true);
            }
        }
        Intent.mouseup = function () { }
    }

    /**Pas de déroulement séquentiel
    geocontrol.intersecte = function (o1, o2) {
        var pt = figure.solveInter(o1, o2);
        if (pt) {plane.objets.push(pt); plane.Selected(pt);}
    }*/

    /**Actions spécifiques et liées à la figure**/
    geocontrol.invertRepereVisible = function () {
        plane.repere.hidden = !plane.repere.hidden;
        plane.redraw();
    }
	
	geocontrol.zoom = function(){
		plane.zoom = !plane.zoom;
		if(!plane.zoom) plane.reset();
		else Intent={name:"zoom",
		  isProperty: true,
		  change: function(pt){
			  var trans = {x:pt.x-plane.canvas.width/4, y:pt.y-plane.canvas.height/4}
			  plane.rescale(2,trans);
			  hasEnded();
		  }
		}
	}

    geocontrol.toggleVisible = function (id) {
        var div = document.getElementById(id);
        with (div.style) { display = (display == 'none' ? 'block' : 'none'); }
    }

    /**field est un champ du viewModel mis à jour en meme temps que l'action**/
    geocontrol.toggleImage = function (img, field) {
        plane[field] = !plane[field];
        img.style.opacity = (plane[field] ? 1.0 : 0.5);
    }

    geocontrol.registerAction = function () {
        geocontrol.undoStack.push(plane.objets().length);
    }

    /*Recherche dabord deux objets sur pt*/
    geocontrol.tryIntersect = function (pt) {
        with (plane) {
            var o1 = selecObjet(pt);
            if (!o1) return false;  //Point ou rien
            var o2 = selecObjet(pt, false, objets.indexOf(o1) + 1);  //Chercher deuxième objet
            if (!o2) return false;
            return geocontrol.tryIntersect2(o1, o2);  //Les deux objets existent
        }
    }

    /*Calcul effectif*/
    geocontrol.tryIntersect2 = function (s, o2) {
        var ptInter = figure.INTERSECTION(s, o2);
        console.log("tryIntersect2");
        console.log(s);
        console.log(o2);
        console.log(ptInter);
        if (ptInter) {
            ptInter.update();  //Sinon, selecPoint sert à rien
            plane.add(ptInter, plane.selecPoint(ptInter));  //Caché si existe;
            if (pt2 = ptInter.brother) {
                plane.add(pt2, plane.selecPoint(pt2));   //Idem!
            }
            geocontrol.registerAction();
            plane.Selected(ptInter);
            return true;
        }
        return false;
    }

    geocontrol.removeAll = function () {
        Intent = { ends: function () { plane.removeAll() } };
        geocontrol.confirmDialog();
    }

    /*Dialogue de type entrer un texte*/
    geocontrol.abandonDialog = function (ident) {
        $(ident).fadeOut(); hasEnded(true);
    }

    geocontrol.enterineDialog = function () {
        var txt = template.inputField.val();
        if (Intent) Intent.ends(txt);           //create
        else if (t = geocontrol.target_dial) {  //update
            figure.updateText(t, txt);
        }
        $("#dialog_geo").fadeOut();
        plane.redraw(1);
    }

    geocontrol.confirmDialog = function () {
        $("#confirm_geo").fadeIn();
        $("#confirm_button").click(function () {
            if (Intent) Intent.ends();
            $("#confirm_geo").fadeOut();
            plane.redraw();
        });
    }

    geocontrol.geoprompt = function (obj,big) {
        if (!obj.helpText) plane.help("Entrez le texte");
        else plane.help(obj.helpText);
        geocontrol.target_dial = obj;
        template.prompt(big);
    }

    /**dépréciée*/
    function geoprompt_(term) {
        $("#dialog_geo").dialog({
            dialogClass: 'no-close',
            buttons: [
            {
                text: "OK",
                click: function () {
                    var value = $("#prompt").val();
                    term(value);
                    $(this).dialog("close");
                }
            },
            {
                text: "Annuler",
                click: function () {
                    $(this).dialog("close");
                }
            }
            ]
        });
    };
}
