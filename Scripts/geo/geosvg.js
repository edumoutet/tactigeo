﻿/*****************************************************************************/
/****Gestion SVG : *******/
function geosvg() {

    var curline = {};
    var cursvg = "";

    function headSVG() {
        var head = '<?xml version="1.0" standalone="no"?>';
        head += '\n<!DOCTYPE svg>';
        return head + "\n" + '<?xml-stylesheet href="styleGEO.css" title="Style SVG" type="text/css"?>';
    }

    function style() {
        return "\n<style>circle,line{fill:none;stroke-width:2px;stroke:blue}" +
               "\ncircle.aspoint{fill:springgreen}" +
               "\ntext{fill:none;stroke-width:1px;stroke:maroon;font-size:12px}</style>";
    }

    //result+="<style>line,path,circle {fill:none;stroke-width:2px;stroke:blue}\n"+
    //" ellipse {fill:none;stroke-width:2px;stroke:red}</style>\n"; 
    var EPS = math.EPS;
    plane.saveSVG = function (standalone) {
        var title = plane.title();
        if (!title) title = "Tactileo géométrie";
        var result = (standalone ? headSVG() : "");
        result += "\n<svg width='" + plane.canvas.width + "' height='" + plane.canvas.height + "' version='1.60'  xmlns='http://www.w3.org/2000/svg'>";
        result += "\n<title>"+title+"</title>" +
                  "\n<description>" + plane.description() + "</description>\n";
        var pts = plane.objets().filter(math.isPoint).filter(figure.isVisible);
        result += '<g style="fill:springgreen;stroke-width:1px;stroke:gray">';  //Les points
        for (nd in pts) {
            var pt = pts[nd];
            if (pt.name == 'REPERE') result += svgRepere(pt);
            else result += "<circle class='aspoint' cx='" + pt.x + "' cy='" + pt.y + "' r='" + EPS / 2 + "'/>\n";
        }
        result += '</g><g style="fill:none;stroke-width:1px;stroke:maroon;font-size:12px">';  //Nom des points
        for (nd in pts) {
            var pt = pts[nd];
            var anno = prim.COLONNE[nd];
            result += "<text id='" + anno + "' x='" + (pt.x - EPS) + "' y='" + (pt.y - EPS) + "'>" + anno + "</text>\n";
        }

        //result += svgRepere(plane.repere);
        result += '</g><g style="fill:none;stroke-width:2px;stroke:gray">';    //Objets
        var objs = plane.objets().filter(figure.isObject).filter(figure.isVisible);
        for (nd in objs) {
            var obj = objs[nd];
            switch (obj.name) {
                case "SEGMENT":
                    result += segline(obj,obj.color);
                    break;
                case "DROITE":  //Var: para, perp
                    result += segline(obj.extrem(),obj.color);
                    break;
                case "CERCLE":
                    result += svgCircle(obj);
                    break;
                case "ANGLE":
                    var adapt = obj.adapt(obj.anno);  //result={centre, [ang1,ang2]}|obj ={a:segment,b:segment},anno[text,pt}}
                    result += svgARC(adapt);
                    if (obj.anno) result += printAnno(adapt.anno);
                    break;
                case "SECTOR":
                    var adapt = obj.adapt();   //srce: SECTOR et result: voir ANGLE
                    result += svgARC(adapt, obj.color, obj.fill);
                    break;
                case "COLLECTION":
                    result += svgPolygon(obj);
                    break;
                case "FUNCTION":
                    var ctx = svgContext;  //Pseudo-contexte svg au lieu de canvas
                    ctx.init(obj.color);
                    if (obj.type != 'polaire')
                        prim.relyFunction(obj.meth, ctx);
                    else prim.relyPolar(obj.meth, ctx);
                    console.log(cursvg);
                    result += cursvg;
                    break;
            }
        }
        return result + "</g></svg>";  //+ "\n<!--" + plane.intelliSave() + "-->";
    }

    /**Satellites**/
    function printAnno(anno)
    {
        with (anno) { return "<text x='" + pt.x + "' y='" + pt.y + "'>" + text + "</text>"; }
    }

    function colorier(color) {
        return " style=\"stroke:" + color + "\"";
    }

    function segline(obj, color) {
        var result = "<line x1='" + obj.a.x + "' y1='" + obj.a.y + "' x2='" + obj.b.x + "' y2='" + obj.b.y + "'";
        if (color) result += colorier(color);
        result += "/>\n";
        if (obj.anno) result += printAnno(obj.annote(true));
        return result;
    }

    function svgCircle(obj) {
        var result = "<circle cx='" + obj.a.x + "' cy='" + obj.a.y + "' r='" + obj.getRayon() + "'";
        if (obj.color) result += colorier(obj.color);
        return result + "/>\n";
    }

    /*Destiné à surcharger prim.relySEGMENT!*/
    function relySegment(pt1, pt2, color) {
        var obj = { a: pt1, b: pt2 };
        cursvg += segline(obj, color);
    }

    /***Requis: adapt={centre:pt,angles:[,],|rayon,orient,petit|}***/
    function svgARC(adapt,color,fill) {
        var result = "";
        trait = color;
        if (fill||!color) { trait = "gray"; }
        function point(P) { return P.x + ',' + P.y; }

        with (adapt) {
            if (!rayon) rayon = 3 * EPS;
            var moreinfos = (typeof (orient) != "undefined");
            var _petit = 0, indirect = 1;
            if (moreinfos) { _petit = petit; indirect = orient; }
            var M1 = math.translate(centre, math.polaire(angles[0], rayon));
            var M2 = math.translate(centre, math.polaire(angles[1], rayon));
            //Params: Pt1/rayX/rayY/Inclinaison=0/petit angle=>0/tjrs et sens indirect=1/Pt2
            var path = ['M', point(centre), 'L', point(M1), 'A', rayon, rayon, 0, _petit, indirect, point(M2), 'L', point(centre)];
            result += '<path d="';
            for (nd in path) result += path[nd] + ' ';
            result += '"' + ' style="stroke:' + trait;
            if (fill) result += '; fill:'+color;
            result += '"/>';
        }
        return result;
    }

    function svgPolygon(poly) {
        var result = "<polygon points=\"";
        var lnks = poly.link;
        for (var np = 0; np < lnks.length; ++np) {
            result += lnks[np].x + ',' + lnks[np].y + ' ';
        }
        result+="\" style=";
        if (poly.fill) result += "\"fill:" + poly.color+";";
        return result + "stroke: gray; fill-opacity:0.5;" + "\"/>";  //pb:rgba()
    }

    function svgRepere(repere) {
        svgContext.init("blue");
        var memWriter = prim.writeMessage;
        prim.writeMessage = svgContext.writeMessage;  //Ecriture dans svg! méthode réécrite car trop incompatible svg
        plane.ctx = svgContext;
        repere.rely(svgContext);
        prim.writeMessage = memWriter;  //Restaure écriture dans canvas
        plane.ctx = plane.gfx();  //Restaure contexte normal
        return cursvg;
    }

    svgContext = {
        init: function (color) { if (!color) color = "gray"; cursvg = "<g style=stroke:"+color+">"; },
        moveTo: function (x, y) { curline.a = {x:x,y:y} },
        stroke: function () { },
        lineTo: function (x, y) {
            curline.b = { x: x, y: y };
            cursvg += segline(curline);
            curline.a = curline.b;
        },
        ends: function () { return cursvg + "</g>" },
        writeMessage: function (text, x, y) {  //Surcharge prim.writeMessage!
            cursvg += "<text x='" + x + "' y='" + y + "'>" + text + "</text>\n";
        }
    }
}



