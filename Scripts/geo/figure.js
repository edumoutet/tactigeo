var functionBuilder = {}
Tactileo.Quiz.figure = function () {
    var EPS = math.EPS;
    var homothetie = math.homothetie;
    var cosAngle = math.cosAngle;
    var distance = math.distance;
    var inclinaison = math.inclinaison;
    var ptcommun = math.ptcommun;
    var polaire = math.polaire;
    var translate = math.translate;
    var reOriente = math.reOriente;
    var petitDirect = math.petitDirect;
    var circ_ordre = math.circ_ordre;
    var intersection = math.intersection;
    var segmentCutCircle = math.segmentCutCircle;
    var projeter = math.projeter;
    var milieu = math.milieu;
    var arrondit = math.arrondit;

    var relySEGMENT = prim.relySEGMENT;
    var relyScaleX = prim.relyScaleX;
    var relyScaleY = prim.relyScaleY;
    var beginStroke = prim.beginStroke;
    var annoteSegment = prim.annoteSegment;
    var arrow = prim.arrow;
    var draw = prim.draw;
    var completeDraw = prim.completeDraw;

    function kindOf(obj) {
        switch (obj.name) {
            case 'SEGMENT':
            case 'DROITE':
                return 'line';
            case 'CERCLE':
            case 'RADIAL':
                return 'circle';
            case 'ARC':
            case 'ARC_EGAL':
            case 'ARC2':
            case 'SECTOR':
                return 'arc';
            case 'FUNCTION':
            case 'Horiz':
                return 'function';
            default:
                return false;
        }
    }

    /**Construction**/
    function DIST(a, b) {
        this.rely = function (ctx) {
            prim.demiEllipse(a, b, ctx);
            var dist = 100 * prim.distance(a, b);
            annoteSegment(a, b, Math.round(dist) / 100);  //2 d�cimales
        }
        this.selec = function () {
            return false;
        }
    }

    function SEGMENT(a, b) {
        this.rely = function (ctx) {
            prim.straight(a, b, ctx);
            //draw(b);
            if (this.anno == 'mesure') this.mesure();
            else if(this.text) this.annote(this.text);
            //else if (this.anno == 'annote') this.annote(plane.getAnno(this));//automatique
        }
        this.selec = function (p) {
            return math.isOnSegment(p, a, b);
        }
        this.mesure = function () {
            var dist = new DIST(a, b);
            dist.rely(plane.ctx);
        }
        this.annote = function (txt) {
            annoteSegment(a, b, txt);
        }
        this.getRayon = function () { return distance(a, b); }

    }

    /*Horizontale variable*/
    function Horiz(a) {
        var left, right;
        this.rely = function (ctx) {
            left = { x: 0, y: a.y };
            right = { x: plane.gridInfo.wid, y: a.y };
            prim.straight(left, right, ctx);
            draw(a);
        }
        this.selec = function (pt) {
            return math.isOnSegment(pt, left, right);
        }
        this.meth = function () { return prim.gfxToCoord(0,a.y).y; }
    }

    function SINE(a, b) {
        this.rely = function (ctx) {
            prim.relySine(a, b, ctx, .1);
            draw(b);
        }
        this.selec = function (p) {
            var cosi = cosAngle(a, p, b);
            return (cosi < -.95);
        }
    }

    /**a,b: points**/
    function DROITE(a, b) {
        var seg = {};  //Elargisssement de [a,b]
        this.name = "DROITE";
        this.rely = function (ctx) {
            seg.a = homothetie(a, 3, b);
            seg.b = homothetie(a, -2, b);
            prim.straight(seg.a, seg.b, ctx);
            if(this.text) annoteSegment(a,b,this.text);
        }
        this.selec = function (p) {
            //var cosi = cosAngle(seg.a, p, seg.b);
            //return (cosi < -.95 || cosi > .95);
            return math.distToLine(p,a,b)<2*EPS;
        }
        this.extrem = function () { return seg; }  //Pour svg!
    }

    function CERCLE(a, b) {
        //{nveau intentSlide: {rayon:...}}
        this.rely = function (ctx) {
            var rayon = distance(a, b);
            ctx.arc(a.x, a.y, rayon, 0, 6.28);
            ctx.stroke();
            if (b.x) { draw(b); }  //Sinon cercle donn� par rayon.
        }
        this.selec = function (p) {
            var rayon = distance(a, b);
            var dist = distance(a, p);
            return (rayon - dist < EPS && rayon - dist > -EPS);
        }
        this.getRayon = function () { return distance(a, b); }
        this.getCenter = function () { return a; }
        this.extra = function () { prim.compas(a, b); }
    }

    function ARC(a, p) {
        this.setRayon = function (rayon) {
            this.rayon = rayon;
            this.update = function () {
                var myray = distance(a, p);			
                if (myray < math.EPS) return true;  //Trop pr�s!
				if(plane.zoom) myray = myray/2;     //Rapport homoth�tie multipli� par 2
                var mypt = homothetie(a, rayon / myray, p);  //Auxilliaire! projette p sur le cercle de rayon this.rayon
                plane.setXY(mypt, p);
                return true;
            }
        }

        this.rely = function (ctx) {
			prim.relyArc(ctx, a, p);
			//prim.draw(p);
        }

        this.selec = function (pt) {
            return math.isOnSmallArc(a, p, pt);
        }

        /**Pour intersections*/
        this.getRayon = function () { return distance(a, p); }

        this.getCenter = function () { return a; }

        this.extra = function () { prim.compas(a, p); }

        p.hidden = true;
    }

    /*Arc de rayon=arc positionn� sur seg cad arc d�pendant d'un arc pr�c�dent*/
    function ARC_EGAL(arc, seg) {
        var ray = 60, myray = 1, mypt=false;
        this.update = function () {
            ray = arc.getRayon();
            myray = distance(seg.a, seg.b);
            if (myray < EPS) return false;
            mypt = homothetie(seg.a, ray / myray, seg.b);  //seg.a->mypt align� sur seg avec longueur ray
            return true;
        }
        this.rely = function (ctx) {
            prim.relyArc(ctx, seg.a, mypt);
        }
        this.selec = function (pt) {
            return math.isOnSmallArc(seg.a, mypt, pt);
        }
        this.getRayon = function () { return ray; }

        this.getCenter = function () { return seg.a; }

        this.extra = function () { if(mypt) prim.compas(seg.a, mypt); }
    }

    /**return ANGLE**/
    function ANGLE(seg1, seg2) {
        if (!ptcommun(seg1, seg2)) return { 
	        invalid: true, why: "Il faut un point commun aux deux segments!!!" };  

        //Retourne donn�es essentielles de l'angle. Pas d'info de rayon.
        this.adapt = function (anno,text) {
            var ndex = ptcommun(seg1, seg2);  //Ex : [a,b] ou[a,a]
            if (!ndex) return false;
            var opps = ['a', 'a'];
            if (ndex[0] == 'a') opps[0] = 'b';
            if (ndex[1] == 'a') opps[1] = 'b';
            var centre = seg1[ndex[0]];
            var a1 = inclinaison(centre, seg1[opps[0]]);
            var a2 = inclinaison(centre, seg2[opps[1]]);

            var ang = math.angleManage(a1, a2);
			var rayon = 3*EPS;
			if(plane.zoom) rayon = 2*rayon;
            result = { centre: centre, rayon:rayon, angles: ang };
            if (!anno&&!text) return result;

            /**Affiche la mesure:*/
            var mil = (ang[0] + ang[1]) / 2;
            var mesure=text;
            if(anno=='mesure') mesure = Math.round((ang[1] - ang[0]) * 180 / 3.14);
            var vec = polaire(mil, 4 * math.EPS);
            var pt = translate(centre, vec);  //display point
            result.anno = { pt: pt, text: mesure };
            return result;
        }

        this.rely = function (context) {
            var adapt = this.adapt(this.anno, this.text);
            with (adapt) {
                context.arc(centre.x, centre.y, rayon, angles[0], angles[1]);
                if (adapt.anno) {
                    //context.strokeStyle = "#555555";
                    context.strokeText(anno.text, anno.pt.x, anno.pt.y);
                }
                context.stroke();
            }
        }

        //c d�signe l'angle courant, p le point dont on veut savoir s'il est sur l'objet!
        this.selec = function (p) {
            var adapt = this.adapt(true);
            if (distance(adapt.anno.pt, p) <= 2 * EPS) return true;
            return false;
        }

    }

    /**return SECTOR : seg est un cercle d�fini par centre et point + champ orient
      p2 est un point d�finissant le secteur circulaire avec les infos de p2.**/
    function SECTOR(seg, p2) {
        var self = this;
        
        self.helpText = "Editez l'angle";

        function updateAngle() {     //Permet � p2 d'imposer l'angle du secteur
            var a1 = inclinaison(seg.a, seg.b);
            var a2 = inclinaison(seg.a, p2);
            self.angle = a2 - a1;    //Si on bouge le point, ce dernier impose l'angle
            self.text = '';
            return (petitDirect(a1, a2) ? 0 : 1);
        }
        self.orient = updateAngle();

		/*Renvoie valeur dans [0..180[*/
		self.valueAngle = function(){
			var result = self.angle;
			if(result<-Math.PI) return result+2*Math.PI;
			if(result>Math.PI) return 2*Math.PI-result;
			if(result<0) return -result;
			return result;
		}
		
        p2.update = function (selected) {//Cela fixe le point sur le cercle!
            if (selected==p2) updateAngle();  //Pt influe sur l'angle		
            if (self.text!=0) {
                self.anno = "mesure";
                self.angle = Math.round(self.text) * Math.PI / 180; //Si on �dite l'angle, l'�dition prend le pas sur angle existant
			    if(self.orient) self.angle = -self.angle;
			}
            var ray = distance(seg.a, seg.b);
            var aux = math.imageRota(seg.a, self.angle, seg.b);
            plane.setXY(aux, p2);
            return true;
        }
        //p2.target = self;  //S�lection! plane.getSelec(... en s�lectionnant p2, va s�lectionner le secteur.

        self.rely = function (context) {
            var centre = seg.a;
            var pt = seg.b;  //pt d�part arc
            var a1 = inclinaison(centre, pt);
           
            context.arc(centre.x, centre.y, distance(centre, pt), a1, a1+self.angle, self.orient);
            context.stroke();
            
            if (this.fill) this.paint(context);
            context.globalAlpha = 1;

            draw(p2); //Point de contr�le
            if (this.anno == 'mesure') this.mesure();
        }

        self.paint = function (ctx) {
            var centre = seg.a;
            var pt = seg.b;
            var a1 = inclinaison(centre, pt);
            var a2 = inclinaison(centre, p2); 
            ctx.beginPath();
            ctx.globalAlpha = .5;
            ctx.fillStyle = self.color;
            ctx.moveTo(pt.x, pt.y);
            ctx.lineTo(centre.x, centre.y);
            ctx.lineTo(p2.x, p2.y);
            ctx.arc(centre.x, centre.y, distance(centre, pt)-1, a1, a2, self.orient);
            ctx.closePath();
            ctx.fill();
        }

        this.mesure = function () {
            prim.writeMessage(prim.degres(self.valueAngle()), p2.x + math.EPS / 2, p2.y - math.EPS / 2);
        }

        self.adapt = function () {
            var centre = seg.a;
            var ptcercle = seg.b;
            var a1 = inclinaison(centre, ptcercle); //Extr�mit�
            var a2 = inclinaison(centre, p2);    //Autre extr�mit�
            var x = petitDirect(a1, a2);
            var petit = (x ? self.orient : 1 - self.orient);  //Ouf!! orient et petit  pour SVG...
            return { centre: centre, angles: [a1, a2], rayon: distance(centre, p2), orient: 1 - self.orient, petit: petit };
        }

        self.selec = function (pt) {
            if (!math.estPetit(distance(pt,seg.a)-distance(seg.b,seg.a))) return false; /*AX=AB=requis?*/
            var se = this.adapt();
            var result = false;
            with (se) {
                var ang = inclinaison(seg.a, pt);
                if (orient)
                    result = circ_ordre(angles[0], ang, angles[1]);
                else result = !circ_ordre(angles[0], ang, angles[1]);
            }
            //if (result) return "collection";
            return result;
        }
        self.getRayon = function () { return math.distance(seg.a,seg.b);}
        self.getCenter = function () { return seg.a}
        //self.extra = function () { prim.compas(cercle.a,cercle.b); }
        return self;
    }

    /**NB: creator sert � s�rialiser puis d�s�rialiser.**/
    /*return point*/
    function MILIEU(seg) {
        var pt = { x: 0, y: 0 };
        pt.creator = { name: "MILIEU", a: seg };
        pt.update = function () {
            var aux = milieu(seg.a, seg.b);
            pt.x = aux.x;
            pt.y = aux.y;
            return true;
        }
        return pt;
    }
    
    /*return syme de a par rapport � b*/
    function SYMECENTRE(a,b){
	    var pt={x:0,y:0}
	    pt.creator={name:"SYMECENTRE",a:a,b:b}
	    pt.update=function(){
		    var aux=homothetie(b,-1,a);
		    pt.x=aux.x;
		    pt.y=aux.y;
		    return true;
	    }
	    return pt;
    }   
    
    /*Idem, b est un axe*/
    function SYMEAXE(a,b){
	    var pt={x:0,y:0}
	    pt.creator={name:"SYMEAXE",a:a,b:b}
	    pt.update=function(){
		    var aux=math.symAxe(a,b);
		    pt.x=aux.x;
		    pt.y=aux.y;
		    return true;
	    }
	    return pt;
    }

    /**a=segment et b=point, result = droite**/
    function PARA(s, b) {
        var a = { x: 0, y: 0 }
        var d = figure.creeObjet("DROITE",a, b);
        d.creator = { name: "PARA", a: s, b: b };
        d.update = function () {
                a.x = b.x + s.b.x - s.a.x;
                a.y = b.y + s.b.y - s.a.y;
            return a;
        }
        return d;
    }

    /**a=segment et b=point, result = droite**/
    function PERP(s, b) {
        var a = { x: 0, y: 0 }
        var d = figure.creeObjet("DROITE",a, b);
        d.creator = { name: "PERP", a: s, b: b };
        d.update = function () {
                a.x = b.x + s.b.y - s.a.y;
                a.y = b.y - s.b.x + s.a.x;
            return a;
        }
        return d;
    }

    /**Actions (pas de cr�ation de point)**/
    function GLUESEGMENT(seg) {
        var self = this;
        self.at = function (pt) { self.x = pt.x; self.y = pt.y; return self; }
        self.update = function () {
            var aux = projeter(self, seg);
            self.x = aux.x;
            self.y = aux.y;
            return true;
        }
    }

    /**Force un point sur un cercle/arc/sector**/
    function GLUECIRCLE(c) {
        var self = this;
        self.at = function (pt) { self.x = pt.x; self.y = pt.y; return self; }
        self.update = function () {
            var cn = c.getCenter();
            var d1 = distance(cn, self);
            var ray = c.getRayon();
            var aux = homothetie(cn, ray / d1, self);  //Projection sur le cercle!
            self.x = aux.x;
            self.y = aux.y;
            return true;
        }
    }

    /**Point coll� sur courbe de fonction**/
    function GLUEFUNCTION(f) {
        var self = this;
        self.at = function (pt) { self.x = pt.x; self.y = pt.y; return self; }
        self.update = function () {
            with (plane) {
                var x = (self.x - repere.x) / plane.scale.dispx;    //NB: plane.scale updated dans prim.relyFunc
                self.y = Math.round(repere.y - f.meth(x) * plane.scale.dispy);
                return true;
            }
        }
    }

    /**Objets attach�s en un point
    plane.addObject(name,a,b,pt) donne x et y
    return point nomm�**/
    function REPERE() {
        var a = { name: 'REPERE', unitx:1, unity:1 };
        //a.creator={name:'REPERE', a: "self"};
         
        a.update = function () {
            arrondit(a, 2 * plane.gridInfo.gridx);
            return true;
        }
        
        a.rely = function (ctx) {
            with (plane.gridInfo) {

                //Axe des ordonn�es
                var pt1 = { x: a.x, y: 0 }
                var pt2 = { x: a.x, y: hei }
                ctx.strokeStyle = "#894";
                prim.straight(pt1, pt2, ctx);
                relyScaleY(a, hei, 2 * gridx,ctx);
                prim.arrow(a, pt1);

                //Axe des abscisses
                var pt1 = { x: 0, y: a.y }
                var pt2 = { x: wid, y: a.y }
                ctx.strokeStyle = "#894";
                prim.straight(pt1, pt2, ctx);          
                relyScaleX(a, wid, 2 * gridx,ctx);
                prim.arrow(a, pt2);
            }
        }
        a.selec = function (p) {
            return false;
        }      
        plane.repere = a;   //Objet unique li� au plan
        plane.updateScale();
        plane.repere.hidden = true;
        return a;
    }

    function TEXT(txt) {
        var pt = { name: "TEXT", text: txt }
        pt.rely = function () {
            prim.writeMessage(pt.text, pt.x, pt.y);
        }
        pt.selec = function (p) {
            var ctx = plane.canvas.getContext("2d");
            return math.boxContains(pt.x, pt.y - 24, ctx.measureText(pt.text).width, 24, p);
        }
        return pt;
    }

    /**Objet sans attache mais d�pendant du rep�re**/
    function FUNCTION(expr, typ) {
        if (!expr || !expr.length) return { invalid: true, why: "Formule absente!" };
        //if (!pas) pas = .1;
        var ftoadd = {};

        ftoadd.name = "FUNCTION";
        ftoadd.helpText = "Entrez expression de x";
        ftoadd.expr = expr;
        ftoadd.type = typ;

        var relyMeth = prim.relyFunction;
        if (typ == 'polaire') {
            relyMeth = prim.relyPolar;
            ftoadd.helpText = "Expression de l'angle x entre 0 et 2pi";
        }

        ftoadd.rely = function (ctx) {
            try { relyMeth(ftoadd.meth, ctx); }  //D�truire la fonction si probl�me
            catch (exc) {
                plane.description("Something wrong on your function!");
                console.log(exc);
                plane.objets.remove(ftoadd);
                //plane.invalid_function = ftoadd;
            }
        }
        if (typ == 'polaire') ftoadd.selec = function (pt) { return false; }
        else ftoadd.selec = function (pt) { return prim.selecFunction(ftoadd.meth, pt); }
        return ftoadd;
    }

    /**A faire d�s la cr�ation de la fonction**/
    function buildRelyMethod(fun) {
        fun.invalid = true;
        try {
            //f.meth = eval("function (x) { with(Math){return " + f.expr + ";}}");
            eval("functionBuilder.meth=function(x){with(Math){ return " + fun.expr + ";}};");
            fun.meth = functionBuilder.meth;
            fun.invalid = false;
            console.log("r�ussite eval?");
            console.log(fun);
            return true;
        }
        catch (exc) { fun.why = 'Mauvaise formulation'; return false; }
    }

    function tryAddFunction(expr, typ) {
        var f = new FUNCTION(expr, typ);
        buildRelyMethod(f);
        if (f.invalid) {
            plane.description(f.why);
            return false;
        }
        else {
            plane.add(f);
            return true;
            //addObject("GLUEFUNCTION", f).at(pt);
        }
    }

    /**act=fill|curve|rely**/
    function COLLECTION(act, tab) {
        if (!act) act = "rely";
        var coll = { name: "COLLECTION", action: act, link: tab }
        coll.color = prim.CRAYON;
        coll.setColor = function (color) {
            coll.color = color;
        }
        coll.selec = function (pt) {
            var G = math.gravityCenter(tab);
            var result = math.distance(pt, G) < 5 * EPS;
            return (result ? 'collection' : false);  //Indique un cas particulier afin de pouvoir s�lectionner un objet int�rieur
            //var d1 = math.polyDistance(tab, pt);
            //var d2 = math.polyDistance(tab, G);
            //return (d1 < 3 * d2 / 2);
        }
        coll.rely = function (ctx) {
            var color = coll.color;
            ctx.strokeStyle = "black";
            if (coll.fill) ctx.fillStyle = coll.color;
            else ctx.strokeStyle = (color ? color : "black");         
            var meth = collection[coll.action];
            meth(tab, ctx, true);
            if (coll.fill) coll.paint(ctx);
        }
        coll.paint = function (ctx) {
            if (coll.color == "pattern") ctx.fillStyle = prim.pattern(ctx);
            else ctx.fillStyle = coll.color;
            ctx.globalAlpha = 0.5;
            ctx.fill();
        }
        return coll;
    }

    var collection = {
        actions: function () { return ["rely", "curve"] }  //pour binding
    }
   
    collection.rely = function (tab, ctx) {
      prim.relyPolygon(tab,ctx,true);
    }

    /*collection.paint = function (tab, ctx) {
        collection.rely(tab, ctx);
        ctx.fill();
        ctx.globalAlpha = 1;
    }*/

    /**Patate : corriger pb closure**/
    collection.curve = function (srctab, ctx) {
        var tab = srctab.concat([srctab[0], srctab[1]]);
        var pt0 = milieu(tab[0], tab[1]);
        ctx.beginPath();
        ctx.moveTo(pt0.x, pt0.y);
        var len = tab.length;
        var mil,ctrl;
        for (var nd = 1; nd < len - 1; ++nd) {  //Au moins trois points
            ctrl = tab[nd];
            mil = milieu(ctrl, tab[nd + 1]);
            ctx.quadraticCurveTo(ctrl.x, ctrl.y, mil.x, mil.y);
        }
        ctx.closePath();
        ctx.stroke();
    }

    /**Courbe (de fonction lisse passant par les points de srctab) d�pr�ci�*/
    collection.sine = function (srctab, ctx) {
        ctx.beginPath();
        for (var np = 0; np < srctab.length-1; ++np) {
            prim.relySine(srctab[np], srctab[np + 1], ctx, 4);
        }
        ctx.closePath();
    }
   
    /**Gestion des intersections***/
    var prior = { line: 0, circle: 1, arc: 2 };
    function priorKind(k1, k2) { return prior[k1] <= prior[k2] }

    var methodHelper = {
        line: { line: 'intersection', circle: 'segmentCutCircle', arc: 'segmentCutArc' },
        circle: { line: 'segmentCutCircle', circle: 'interCircle', arc: 'circleCutArc' },
        arc: { line: 'segmentCutArc', circle: 'circleCutArc', arc: 'arcCutArc' },
        "function":true
    }

    /**Aspect math�matique...*/
    function INTERSECTION(o1, o2) {
        var k1 = kindOf(o1), k2 = kindOf(o2);
        if (!k1 || !k2 || !methodHelper[k1]) return false;
        if (k1 == "function" && k2 == "function") return InterFunc(o1, o2);

        var methodName = methodHelper[k1][k2];
        if (!methodName) return false;

        var pt = { x: 0, y: 0 }  //R�sultat!
		
        if (math.ptsEgaux(o1, o2) && methodName == 'segmentCutCircle')//Ne g�re pas tous les cas!
            methodName = 'symCentrale';
		else if (methodName == 'segmentCutCircle' || methodName == 'interCircle')
		    pt.brother = { x: 0, y: 0, name: "dontload" }
		var method = math[methodName];	
        
        pt.update = function () {
            var pts;
            if (priorKind(k1, k2)) pts = method(o1, o2); //PB signature m�thode
            else pts = method(o2, o1);
            if (!pts) return false;
            else if (pts.length) {
                plane.setXY(pts[0], pt);
                plane.setXY(pts[1], pt.brother);
                return true;
            } else 
				plane.setXY(pts, pt);  //pt prend les coord de pts
            return true;
        }
        pt.creator = { name: 'INTERSECTION', a: o1, b: o2 }
        return pt;
    }

    /*Exp�rimental*/
    function InterFunc(f1, f2) {
        var obj = {
            a:f1, b:f2,
            name: "InterFunc",
            zeros: [],
            selec: function(pt){return false}
        };

        obj.update = function () {
            var interval = prim.findInterval(0, plane.repere.x, plane.gridInfo.wid);
            var diff = function (x) { return f1.meth(x) - f2.meth(x); }
            obj.zeros = math.findZero(diff, interval);  //Dynamique!
            return true;
        };

        obj.rely = function (ctx) { obj.zeros.forEach(function (z) { prim.draw(prim.coordToGfx(z, -f1.meth(z))); }); };
         
        obj.selec = function (pt) {
            if (math.getCloser(pt, obj.zeros)) return true;
            return false;
        }

        return obj;
    }

    /*Quand text ne suffit pas tout seul*/
    function updateText(obj, txt) {
        if (obj.name == 'FUNCTION') {
            obj.expr = txt;
            buildRelyMethod(obj);
        }
        /*else if (obj.target) {
            obj.target.text = txt;  //Sector
            obj.text = "";
        }
        else obj.text = txt;*/
        else if (obj.name == "Message") $("#text" + obj.ref).html(txt.replace(/\n/g, '<br>'));
    }

    /**Pour les mod�les non conformes � construct(a,b)**/
    //var rep = new REPERE(); rep.x = obj.point.x; rep.y = obj.point.y; rep.hidden = obj.hidden; return rep; }
    var loader = {}
    loader["TEXT"] = function (obj) { return new TEXT(obj.property['text']); }
    loader["REPERE"] = function (obj) { return new REPERE(); }  
    loader["FUNCTION"] = function (obj) {
        var f = new FUNCTION(obj.property['expr'], obj.property['type']);
        console.log("loader function");
        console.log(f);
        buildRelyMethod(f);
        return f;
    }
    loader["COLLECTION"] = function (obj) { return COLLECTION(obj.property['action'], obj.link); }
    loader["RADIAL"] = function (obj) { return new RADIAL(obj.a, obj.property['rayon']); }

    figure = {
        call: function (name) { return eval(name); },
        tryAddFunction: tryAddFunction, updateText: updateText,
        SEGMENT: SEGMENT, DROITE: DROITE, PARA: PARA, PERP: PERP, DIST: DIST, SINE: SINE, Horiz: Horiz,
        CERCLE: CERCLE, SECTOR: SECTOR, ANGLE: ANGLE, 
        MILIEU: MILIEU, ARC: ARC, ARC_EGAL: ARC_EGAL,
        GLUESEGMENT: GLUESEGMENT, GLUECIRCLE: GLUECIRCLE, GLUEFUNCTION: GLUEFUNCTION,
        REPERE: REPERE, TEXT: TEXT, FUNCTION: FUNCTION, COLLECTION: COLLECTION, collection: collection, 
        SYMECENTRE:SYMECENTRE, SYMEAXE:SYMEAXE,
        listProperties: listProperties, kindOf: kindOf, loader: loader, interHelper: INTERSECTION, INTERSECTION: INTERSECTION,
    }

    figure.isObject = function (obj) {
        return (obj && !obj.hasOwnProperty('x'));
    }

    figure.isVisible = function (obj) { return !obj.hidden; }

    figure.creeObjet = function (name, a, b) {
        var ctrsct = figure[name];
        var obj = new ctrsct(a, b);  //Cr�e objet tant script(obj.creator - ex: para) que physique(ex: segment)
        //if (obj.end) { obj.end(); }  //NEW
        if (!obj.creator) {  //Sinon, g�r� par creator! ici: objet physique.
            obj.a = a;
            obj.b = b;
            obj.name = name;
        }
        return obj;
    }

    /*Dit quelles propri�t�s sont valides - name: type objet*/
    var property = function (name) {  
        if (!name) return ["edit_text","coord"];
        var result = [];
        switch (name) {
            case "SEGMENT":
                result = ["pointille", "mesure", "edit_text", "color"]; break;
            case "DROITE":
                result = ["pointille", "edit_text", "color"]; break;
            case "ANGLE":
                result = ["mesure", "edit_text"]; break;
            case "ARC":
                result = []; break;
            case "CERCLE": 
                result = ["pointille", "color"]; break;
            case "FUNCTION":
                result = ["edit_text", "pointille", "color"]; break;
            case "RAPPORTEUR":
                result = ["color", "mesure", "fill", "edit_text"]; break;
            case "SECTOR":
                result = ["color", "hachure", "mesure", "fill", "edit_text"]; break;
            case "TEXT":
                result = ["edit_text", "color"]; break;
            case "COLLECTION":
                result = ["color", "fill", "hachure"]; break;
            case "SINE":
                result = ["color"]; break;
            case "Message":
                result = ["edit_text"]; break;
            case "REPERE":
                break;
            default:  //Point 
                result = ["edit_text", "coord"];
        }
        return result;
    }

    function hasProperty(obj, prop) {
        return property(obj.name).indexOf(prop) != (-1);
    }

    /**s: objet s�lectionn�**/
    function listProperties(s) {
        return  property(s.name); 
    }
    
    /**fonctions-propri�t�s**/
    figure.fill = function (obj) {
        obj.fill = !obj.fill;
        if (!obj.color) obj.color = "#eee";
    }

    figure.annote = function (obj) {
        obj.anno = 'annote';
    }

    figure.mesure = function (obj) {
        obj.anno = 'mesure';
    }

    figure.edit_text = function (obj) {
        if (obj.coord) obj.coord = false;
        var big = (obj.name=='Message');
        geocontrol.geoprompt(obj,big);  //obj est la cible!
        obj.anno='text';
        //obj.text = prompt("Entrez le texte:");
    }

    figure.hachure = function (obj) {
        obj.color = "pattern";
    }

    figure.pointille = figure.hachure;

    figure.lock = function (obj) {
        obj.locked = true;    //point seulement;
    }
	
	figure.coord=function(obj){
		obj.coord=true;
	}

	figure.trace = function (obj) {
	    plane.dotrace(obj);
	}

    /*Place un message(elem html) au-dessus du canvas + cr�e objet Message*/
	figure.htmlMessage = function (x, y, text) {
	    var ref = $(".text").length;
	    text=text.replace(/\n/g,'<br>');
	    $("#canvasContainer").append("<div class='text' id='text" + ref + "' style='left:" + x + "px;top:" + y + "px' onclick='plane.getMessage(this.id)'>" + text + "</div>");
	    $(".text").draggable();
	    return { name: "Message", ref: ref, hidden: true };
	}

	figure.addMessage = function (ref) { plane.add({ name: "Message", ref: ref, hidden: true }); }  //Objet de travail
	
     /*cf plane.getMessage qui s�lectionne un message->depre*/
	figure.getMessageContent = function (msgId) {
	    var elem = $("#" + msgId);
	    var txt = elem.html();
	    return txt.replace(/\<br\>/g,'\n');
	}

	figure.saveMessage = function(msgid) {
	    var elem = $("#text" + msgid);
	    var x = elem.css("left").replace("px", "");
	    var y = elem.css("top").replace("px", "");
	    var msg = elem.html();
	    return { name: "Message", point: { x: x, y: y }, property: [{key:"text",value:msg}], hidden:true };  //Objet du service
	}//todo: mettre text en property

	figure.loader["Message"] = function (obj) {
	    var point = obj.point;
	    return figure.htmlMessage(point.x, point.y, obj.property["text"]);
	}
	
	 /*Tentative de comparaison*/
	function areSame(obj1,obj2){
		if(!obj1) return !obj2;  //Nuls tous deux!
		//console.log("non nuls tous deux!");
		/*console.log("obj1|obj2");
		console.log(obj1);
		console.log(obj2);*/
	    if(obj1.name!=obj2.name) return false;
	    if(!areSame(obj1.creator, obj2.creator)) return false;
	    if(!areSame(obj1.a,obj2.a)) return false;
	    if(!areSame(obj1.b,obj2.b)) return false;
	    return true;
	}
	
	figure.areSame = areSame;
};