﻿Tactileo.Quiz.plane=function () {
    plane = {
        canvas: false,
        mode:'analytic',  //euclid vs analytic

        version: 2.0,
        zoom: false,

        description: ko.observable("Créez avec la barre d'outils"),
        help: ko.observable(),
        title: ko.observable("Titre"),
        image:ko.observable(),
		script: ko.observable(),  //Script géré par mousedown/mouseup

		objets: ko.observableArray(),
		add: function (obj, hidden) {
		    plane.objets.push(obj); if (hidden) obj.hidden = true;
		},
		remove: function (obj) { obj.hidden = true; },
		Selected: ko.observable(false),

        pointAutoName:ko.observable(true),
        gridset: ko.observable(false),
        pdown: false,        //Point mobile
        mouseup: true,  //Pour forcer ?v?nement mousedown [mouseup si false]
        
        trace:[],
        invalid_function:false,
        gfx: function () { return plane.canvas.getContext("2d"); },

        listProperties: function () {
            var s = plane.Selected();
            if (s && !s.discret) return figure.listProperties(s);
            return [];
        },
        
        setProperty: function (name) {
            var meth = figure[name];
            meth(plane.Selected());
            plane.redraw(1,1);
        },
        
        isPoint: math.isPoint,

		rule:ko.observable(100),
		hasRule:ko.observable(),  //met:regler; demet:geocontrol.hasEnded
		regler:function(){
			plane.hasRule(true);
			var offsetx=$("#slider").width()/2;
			if(d=plane.rule()) $("#slider").css("left",Math.round(d)-offsetx+'px');
			prim.drawRule(document.getElementById("canslider"),480);
			plane.description("Arc de rayon fixé par la règle");
			$("#slider").draggable({axis:'x', stop:function(evt,ui){
			    //console.log("!!!"+ui.position.left);
				plane.rule(ui.position.left+offsetx);
				}});}  
    }

    var kindOf = figure.kindOf;
	var isPoint = math.isPoint;

    var egalite = math.egalite;
    var entier = math.entier;

    var draw = prim.draw;
    var beginDraw = prim.beginDraw;
    var completeDraw = prim.completeDraw;
    var SELCOLOR = prim.SELCOLOR;

    ko.bindingHandlers.selText = {
        update: function (element, valueAccessor, allBindings) {
            var value = valueAccessor();
            var target = plane.Selected();
            target.text = value;
        }
    }

    ko.bindingHandlers.placeholder = {
        update: function (element, valueAccessor, allBindings) {
            var value = valueAccessor();
            //console.log(ko.unwrap(value));
            $(element).attr("placeholder", ko.unwrap(value));
        }
    }
	
    function createCanvas(where, wid, hei) {
        var canview = "<canvas id='geocanvas' width='" + wid + "' height='" + hei + "'/>";
        $(where).append(canview);
    }
	
	function isDefined(obj){
		return(typeof(obj)!='undefined');
	}

    /*Presque constant: permet de régénérer simplement grille initiale*/
	function stdGridInfo() { with (plane) plane.gridInfo = { wid: canvas.width, hei: canvas.height, gridx: 30, gridy: 30 };};

    plane.initCanvas = function (retime) {
        with (plane) {
            canvas = document.getElementById("geocanvas");
            if (canvas == null) {
                if (retime) { setTimeout(function () { plane.initCanvas() }, retime); }
                else { alert("Erreur: pas de canvas dans la page!"); }
                return;
            }
            //wid = canvas.width;
            //hei = canvas.height;
            var evts = detectmob() ? ['touchstart', 'touchmove', 'touchend'] : ['mousedown', 'mousemove', 'mouseup'];
            canvas.addEventListener(evts[0], mousedown, false);
            canvas.addEventListener(evts[1], updateCanvas, false);
            canvas.addEventListener(evts[2], emulemouseup, false);
            stdGridInfo();
            plane.ctx = canvas.getContext("2d");
        }
    }

    plane.detectmob = function () {
        if (navigator.userAgent.match(/Android/i)
           || navigator.userAgent.match(/webOS/i)
           || navigator.userAgent.match(/iPhone/i)
           || navigator.userAgent.match(/iPad/i)
           || navigator.userAgent.match(/iPod/i)
           || navigator.userAgent.match(/BlackBerry/i)
           || navigator.userAgent.match(/Windows Phone/i)
           ) {
            return true;
        }
        else {
            return false;
        }
    }

    plane.defaultEnd = function (evt) {
        var sel = plane.Selected();
        if (!isPoint(sel)) return;
        var pt2 = plane.selecPoint(sel, sel);  //param1: coordonnées et param2: dit qu'on évite de resélectionner sel!
        if (pt2) plane.reaffecte(sel, pt2);
    }
    /*console.log("try add glue"); //faire glue ?
				console.log(mem);
				console.log(sel);
				if(mem && !mem.update) {  //Va placer mem sur sel
					var glued=addGlueOn(touchXY,sel);
					if(glued) reaffecte(mem,glued);
				}*/

    //var interExist = false;  //Besoin pour interdire reaffecte si intersection(pb avec deux points)

    plane.mousedown = function (evt) {
        with (plane) {
            pdown = getInfos(evt);
            interExist = false;
			plane.script("pen down:"+pdown.x+':'+pdown.y);
            //var exist = (pdown == Selected());  //Pt sélectionné
            if (Intent) { Intent.change(pdown); return; }
            else plane.defaultAction(pdown);  //Intersection par ex
        }
    }

    plane.emulemouseup = function (evt) {
		with (plane){
	    if (gridset()) math.arrondit(pdown, gridInfo.gridx);  //NB: pas utile sur point lié
		var tit = script();	
		    tit+='|'+"pen up";
		    if(pdown) tit+=':'+pdown.x+':'+pdown.y;
		    script(tit);
		    if (Intent) Intent.mouseup();  //Utilise pdown
		    else {
		        if (!interExist) plane.defaultEnd(evt);
		        mouseup = true;
		        pdown = false;
		        redraw(1, 1);
		    }
		}
    }
	
	//Employé par exeScript pour simuler un glissement de pdown à pt
	plane.moveto = function (pt) { plane.setXY(pt, plane.pdown); plane.emulemouseup(pt);}  
	
	//plane.setRule=function(r){return plane.gridInfo.gridx*r.value+'px'}  //Valeur css left du composant slider
	
	plane.exeScript=function(script){
		if(!script.length) return;
		var actions=script.split('|');
		var cdown=actions[0].split(':');  //point 1
		plane.mousedown({x:Math.round(cdown[1]), y:Math.round(cdown[2])});
		if(actions.length){
		  var cup= actions[1].split(':');  //point 2 éventuel(glissement)
		  if(cup.length>1) plane.moveto({x:Math.round(cup[1]), y:Math.round(cup[2])}); //2 coords en princip!
		}
		else plane.emulemouseup(false);  //snd point sans intérêt
		//plane.redraw();
	}

    /*mode onlyclick*/
    plane.clickend = function (evt) {
        plane.pdown = plane.getInfos(evt);
        plane.emulemouseup(false);
    }

    plane.cntupdate = 0;  //Pour éviter update trop fréquent!

    /***Méthode mousemove. Se base sur pdown pour modifier un point.***/
    plane.updateCanvas = function (evt) {
        with (plane) {
            if (!pdown || pdown.locked) return;  //Cela empeche redraw avec point mobile. Permet => message affich?.  //Empeche un pt locké de bouger
            var touch = getTouch(evt);
            cntupdate = (cntupdate + 1) % 10;

            //if (gridset()) math.arrondit(touch, gridInfo.gridx);
              pdown.x = touch.x;
              pdown.y = touch.y;
            if (cntupdate) prim.draw(touch, prim.LIGHTCOLOR);
            else {
                //if(Intent && Intent.update) Intent.update(pdown,touch);    
                redraw(1);  //Keep Selected;
            }
        }
    }

    /*Traitement primitif*/
    plane.getTouch = function (evt) {
        evt.preventDefault();
        var rect = plane.canvas.getBoundingClientRect();
        var pts = evt.changedTouches;
        var touchXY;
        if (!evt.touches)
            touchXY = {   //Classic event
                x: evt.clientX - rect.left,
                y: evt.clientY - rect.top
            };
        else
            touchXY = {    //Mobile event
                x: pts[0].pageX - rect.left,
                y: pts[0].pageY - rect.top
            };
        return entier(touchXY);
    }

    /****Traitement du point cliqué(sélection)
	****/
    plane.getInfosPoint=function(touchXY){  
        with (plane) {
            if (gridset())
                math.arrondit(touchXY, gridInfo.gridx);
            var presult = selecPoint(touchXY);  
            if (presult) {            
                Selected(presult);
                //if (tar = presult.target) Selected(tar);  //Ex pour secteur mais pb: point ne peut plus bouger(voir figure.SECTOR)
				pdown=presult;
                return presult;
            }
            Selected(false);
            if (sel = selecObjet(touchXY, false, 0)) {  /*!pdown*/
                Selected(sel);
            }
        }
        return touchXY;
    }
	
	//Actions touch/souris: par défaut!
    plane.defaultAction = plane.getInfosPoint;  //Tentative de sélection
    
    /*Pour scripts/evenement: accepte evt=evt click OU {x:,y:}**/
    plane.getInfos = function (evt) {
		//Si evt ={x:,y:} on ne traite pas (on a affaire à un script et non un évènement)
        var isTouchEvent = evt.type; //evt.isTrusted||evt.touches; //evt.toString().search("MouseEvent") > -1;
        var touchXY = isTouchEvent ? plane.getTouch(evt) : evt;
		return entier(touchXY);
        //return plane.getInfosPoint(touchXY);
    }

    /**Méthodes d'ajout**/
    /**recherche point par coordonnées semblables(selecPoint)*/
    plane.addIfMisses = function (pt) {
        var exist = plane.selecPoint(pt);
        var firstP = (exist ? exist : pt);
        if (!exist) plane.addPoint(firstP);
        return firstP;
    }

    plane.addPoint = function (pt)
    {
	    with(plane){
          if (gridset()) math.arrondit(pt, gridInfo.gridx);
          add(pt);
        }
    }

    /**attached aura les coordonnées de pt**/
    plane.addAt = function (attached, pt) {
        with (plane) {
            setXY(pt, attached);
            add(attached);
        }
    }

    //Essaie de sélectionner un point
    plane.selecPoint = function (p, pinterdit) {
        function authPoint(po) {
            return isPoint(po) && (po != pinterdit) && (!po.hidden);
        }
        var tab = plane.objets().filter(authPoint);
        return math.getCloser(p, tab);
    }
    /*plane.selecPoint = function (p,interdit) {
        var tab = plane.objets();
        for (var nd in tab) {
            var pt = tab[nd];
			if(pt==interdit) continue; //bi-sélection!
            //if (pt.locked) continue;
            if (p == pt) return pt;
            if (pt.hidden) continue;
            if (!isPoint(pt)) continue;
            if (pt.selec && pt.selec(p)) return pt;  //point évolué
            if (egalite(pt, p)) return pt;  //point primitif->regarder existence pt.selec?
        }
        return false;
    }*/

    //Idem avec un objet
    plane.selecObjet = function (p, pref, ndepart) {
        var tab = plane.objets();
        var found=false, info;
        for (var nd = 0 | ndepart; nd < tab.length; ++nd) {
            var obj = tab[nd];
            if (isPoint(obj) || obj.hidden) continue;        
            if (info = obj.selec(p)) {
                if (!pref || pref == kindOf(obj))  //A cause des Intent et des objets superposés 
                {
                    prim.selecShow(obj, "", "orange");
                    if (obj.getRayon) plane.rule(obj.getRayon());
                    if (info != 'collection') return obj;
                    else found = obj;
                }
            }
        }
        return found;  //Si on a une collection, but: sélectionner objet interne à une collection 
    }
	
	plane.selecGeneric=function(p,kind){
		if(!kind) return plane.selecPoint(p);
		return plane.selecObjet(p,kind);
	}

    //Utilitaire pour Intent: renvoie Selected si on a bien le type s?lectionn? kind. 
    //NB: kind peut ?tre identique pour deux types diff?rents: ex: droite et segments ont kind = line
    plane.getSelec = function (kind) {
        with (plane) {
            if (!(s = Selected())) return false;  //Sélection

            if (isPoint(s) && (!kind)) {
                draw(s, prim.SELCOLOR);
            }
            else {
                if (kindOf(s) != kind) s = plane.selecObjet(s, kind);  //Recherche sélection compatible avec kind
                if (s) completeDraw(s, prim.SELCOLOR);
            }
            return s;
        }
    }
    
    plane.canFix = function () {
        var pt = Selected();
        if (isPoint(pt)) {
            var sel = selecObjet();
            var kind = kindOf(sel);
            if (kind == "SEGMENT" || kind == "CIRCLE") return true;
        }
    }

	plane.abandon = function(){
	    with (plane) {
	        if (trace.length) resetTrace();  //Ne pas supprimer le point tracé
	        else if (!Intent) {
	            var s = Selected();  //Incorrect si Intent
	            if (s) {
	                s.hidden = true;
	                if (s.name == "Message") {  //Message est affiché en mode html
	                    $("#text" + s.ref).css("display", "none");
	                    s.creator = "dontsave";
	                }
	            }
	        }
		release();
		redraw();
		}
	}
	
    plane.release = function (keepSelected) {
        with (plane) {
            Intent = false; 
            mouseup = true; 
            pdown = false;  
            if (!keepSelected) {
                Selected(false);  
            }
            description(false);
			hasRule(false);
        }
    }

    /*Dépréciée: cf geocontrol.removeAll()*/
    plane.effacer = function () {
        if (!confirm("Voulez-vous effacer le dessin en entier?")) return;
        plane.removeAll();
    }

    plane.removeAll = function () {
        console.log("plane.removeAll!");
        with (plane) {
            objets.removeAll();
            $(".text").remove();  //Messages!
			if(figure.REPERE)
              plane.addAt(figure.REPERE(), { x: 200, y: 150 });
            release();
            redraw();
        }
    }

    /**Pour dessiner une figure en traits épais.*/
    prim.setDrawParams();

    plane.creation=function(message){
        plane.Selected(false);
        plane.description(message);
    }

    plane.redraw = function (keepselected, keepdescription) {
        with (plane) {
            var selected = Selected();
            if (selected && selected.name != 'REPERE') plane.description(selected.name||"point");  //Afficher bouton de suppression! mieux que @

            /*NB: couleur sera géré par prim.draw*/
            function tryDraw(obj, surligne) {
                if (obj.update) { if (!obj.update(selected)) return;}
                if (obj.hidden||obj.discret) return;    //Faire quand même update!
                if (obj.rely) { completeDraw(obj, surligne);}
                else if (plane.pointAutoName()) prim.draw(obj, surligne, npt++);   //NB: point obligatoirement
                else prim.draw(obj, surligne, -1);
            }
            
            gfx().font = prim.font;
            prim.drawGrid(plane.gridset());

            var npt = 0;  //Besoin dans tryDraw!

            var tab = objets();
            tab.forEach(function (obj) { tryDraw(obj, false); });
           
            /**Surligne sélection*/
            npt = -1;
            if (selected) tryDraw(selected, true);
            
            /*En cas d'échec de fonction tardif*/
            if (invalid_function) {
                objets.remove(invalid_function);
                invalid_function = false;
                console.log("invalid object removed");
            }

            if (!keepdescription) description(false);
            if (!keepselected) Selected(false);

            /*if (trace.length >= 64) plane.resetTrace();  
            trace.forEach(function (tr) {
                prim.draw(tr, "gray", -1);
            });*/
			
            /**Cas d'un objet pas encore ajouté à la liste d'objets, pendant la construction par une Intent par exemple*
            if(Intent && (obj=Intent.obj)){
                tryDraw(obj,SELCOLOR);
            }*/

        }
    }
	
	plane.ruleHelp = function(pt){
		var ctx = plane.gfx();
		ctx.beginPath();
	    ctx.setLineDash([5,15]);
		ctx.arc(pt.x,pt.y,plane.rule(),0,6.28);
		ctx.stroke();
	}

    plane.redrawAfter = function (field) {
        var b = plane[field]();
        plane[field](!b);
        plane.redraw();
    }

    plane.animate = function () {
        plane.creation("Animation");
        prim.drawGrid();
        var npt = 0;
        var nobj = 0;
        var tab = plane.objets();

        var draw1 = false;
        var timer = setInterval(drawIteratif, 500);
        $(".text").css("display", "none");  //Messages

        function drawIteratif() {
            if (nobj == tab.length) { clearInterval(timer); return; }

            var obj = tab[nobj];
            draw1 = !draw1;
            if (!draw1)++nobj;

            if (obj.name == "Message") $("#text" + obj.ref).css("display", "block");  //Avant test suivant car messages sont hidden (objets virtuels)
            if (obj.hidden) return;

            var color = draw1 ? "pattern" : false;

            if (obj.rely) completeDraw(obj, color ? color : obj.color ? obj.color : false);       
            else prim.draw(obj, false, draw1 ? -1 : npt++);   //NB: point obligatoirement  
            if (obj.extra) obj.extra();
        }
    }

    plane.dotrace = function (obj) {
        var update = obj.update;  //Ne tracer que objet avec update
        if (!update) return;
        obj.update = function () {
            update();
            plane.trace.push({ x: obj.x, y: obj.y });
        }
        plane.resetTrace = function () {
            obj.update = update;
            plane.trace = [];
        }
    }

    //Supprime l'objet s?lectionn?, s'il existe
    plane.supprimer = function () {
        with (plane) {
            if (!(s = Selected())) return;
            objets.remove(s);
            Selected(false);
            redraw();
        }
    }

    plane.getAnno = function (obj) {
        var nd = plane.objets.indexOf(obj);
        return prim.charAtIndex(nd).toLowerCase();
    }

    plane.translate = function (vec) {
        var tab = plane.objets();
        var pt;
        for (nobj in tab) {
            if (isPoint(pt = tab[nobj])) { pt.x += vec.x; pt.y += vec.y; }
        }
    }

	//pt=aux byval
    plane.setXY = function (aux, pt) {
        pt.x = Math.round(aux.x);
        pt.y = Math.round(aux.y);
    }

    plane.selColor = function (color) {
        if (s = plane.Selected()) {
            console.log("selColor=>color&s:");
            console.log(color);
            console.log(s);
		if (s.setColor) s.setColor(color); 
		else s.color = color;
        plane.redraw(1);
		} 
	}

    plane.lockAll = function () {
        plane.creation("Verrouille les points");
        plane.objets().forEach(function (obj) { if (isPoint(obj)) obj.locked = true; });
    }

	/**Cherche à coller ptoadd sur objet sélectionné. Si échoue, ajoute ptoadd au modèle**/
    plane.addGlue = function (ptoadd) {
        with (plane) {
            var sel = selecObjet(ptoadd);
            var result = false;
            if (sel) result = addGlueOn(ptoadd, sel);
            if (!result) { add(ptoadd); return ptoadd; }
            return result;
        }
	}
	
	/*return pt englué sur sel at l'ajoute|false*/
	plane.addGlueOn = function(ptoadd,sel){
        var type = figure.kindOf(sel);
        var methname = false;
        console.log("plane.addGlueOn:" + type);
        switch (type) {
            case 'line': methname = 'GLUESEGMENT'; break;
            case 'circle': case 'arc': methname = 'GLUECIRCLE'; break;
            case 'function': methname = 'GLUEFUNCTION'; break;
        }
        if (methname) {
            var glued = plane.addObject(methname, sel);
            glued.at(ptoadd);  //Ajout d'un point englué
            return glued;
        }
		return false;     
    }
	
	plane.addObject=function(name, p1, p2) {
        with (plane) {
            var obj = figure.creeObjet(name, p1, p2);
            if (obj.invalid) { console.log("Objet invalide: " + obj.why); return false; }  //Erreur lors de la création
            Selected(obj);
            add(obj);
            if (obj.brother) add(obj.brother);
            return obj;
        }
    }

    //ptokill remplacé par ptokeep
    //Usage normal: on a raté un point en tapant, on veut supprimer celui créé par erreur
    plane.reaffecte = function (p1, p2) {
        with (plane) {
			console.log("Essai réaffectation!");
            var ptokill = false;
            var ptokeep = false;
            var valid1 = (!p1.update && !p1.rely);
            if (valid1) { ptokill = p1; ptokeep = p2; } //Recherche pt simple 
            else {
                return;
                //var valid2 = (!p2.update && !p2.rely);  //Recherche pt simple 
                //if (valid2) { ptokill = p2; ptokeep = p1; }
                //else return;   //Il faut un point simple au moins!
            }
            console.log("Réaffectation");
            var tab = objets();  //Modifier les liaisons: utile pour sauvegarde mais pas pour le présent à cause des objets "finaux" ie param a,b non modifiables pour le dessin
            tab.forEach(function (obj) {
                if (obj.a == ptokill) obj.a = ptokeep;
                if (obj.b == ptokill) obj.b = ptokeep;
            });
            ptokill.creator = "dontsave";  //Pour n'en sauvegarder qu'un en fait, ptokill sera gardé pour comptage correct mais pas svgardé
            ptokill.hidden = true;
            ptokill.update = function () { //Voir rqe précédente - hidden ne suffit pas à cause des liaisons tjrs présentes ie ptokill tjrs référencé: ne pas suppr ptokill! 
                ptokill.x = ptokeep.x;
                ptokill.y = ptokeep.y;
            };
            var nkeep = objets.indexOf(ptokeep);
            var nkill = objets.indexOf(ptokill);
			//keep doit avoir index < killed à cause des liaisons
            if (nkeep > nkill) { var mem = tab[nkeep]; tab[nkeep] = tab[nkill]; tab[nkill] = mem; }  
			return ptokeep;
        }
    }

    /*zoom-Alternative: plane.gfx().transform?*/
    plane.rescale = function (scfactor,trans) {
        with (plane.gridInfo) {
            gridx = Math.floor(gridx * scfactor);
            gridy = Math.floor(gridy * scfactor);
            plane.updateScale();
            wid = Math.floor(wid * scfactor);
            hei = Math.floor(hei * scfactor);
			var pts = plane.objets().filter(isPoint);
			pts.forEach(function (pt) {
				pt.memo = {x:pt.x,y:pt.y}  //Pour retrouver point initial!
				if(trans) {
					pt.x-=trans.x;
					pt.y-=trans.y;}
				pt.x = Math.floor(pt.x * scfactor);
				pt.y = Math.floor(pt.y * scfactor);
			});
		}    
    }

    /*Défait le zoom!*/
    plane.reset = function () {
        stdGridInfo();
        plane.updateScale();
        var pts = plane.objets().filter(isPoint);
        pts.forEach(function (pt) {
            plane.setXY(pt.memo, pt);
        });
        plane.redraw();
    }

    /*Appeler à tt changement unité/grille*/
    plane.updateScale = function () {
        with (plane) { plane.scale = { dispx: gridInfo.gridx * 2 / repere.unitx, dispy: gridInfo.gridy * 2 / repere.unity } }
    }

    /*Tente de sélectionner un message...évènement alternatif au classique touch ident=id elem html du message, ex:text4*/
	plane.getMessage = function(ident){
	    var msgSelec = plane.objets().filter(function (obj) { return "Message" == obj.name&&(ident="text"+obj.ref) });
	    if(msgSelec){
		        msgSelec=msgSelec[0];
		        msgSelec.text = figure.getMessageContent(ident);
	            plane.Selected(msgSelec);
	            plane.description("Message...");
	        }
	    return msgSelec;
	    //if (Intent&&msgSelec) Intent.change(msgSelec); expérimental!
	}
	
	/**Fonctions override possible(geoloader)*/
	plane.save=function(){return {liste:JSON.stringify(plane.objets())}}
	
	plane.load=function(recep){plane.objets(JSON.parse(recep));}
	
	/**File management: ajax(service url,directory,filename)**/
	plane.toFile = function (filemanager,dir,filename) {  //Ajax save!
        if (!filename) { alert("Renseigner un nom!"); return; }
        var envoi = plane.save();  //{liste,figure_option,name,_id}
		envoi.dir = dir;
        envoi.filename = filename;
        envoi.goal = 'save'; //{ liste, filename, goal }
        $.post(filemanager, envoi).done(function (liste) { manager.log.value = liste; });
    };
	
	plane.fromString=function(strJson){
		 if(strJson && strJson.length){
		 var recep = JSON.parse(strJson);
         plane.load(recep);
		 }
	}
	
	plane.fromFile = function (filemanager,dir,filename) {
        var envoi = { goal: "load", filename: filename, dir: dir };
		console.log(envoi);
        $.post(filemanager, envoi).done(plane.load);
	}
	
	 /*Compare 2 listes d'objets et retourne vrai si tous elems de needed sont dans scene=>supporte les artefacts*/
	plane.requireScene=function(needed) {
		var scene=plane.objets();
		//needed=JSON.parse(needed);
	    if (needed.length > scene.length) return { resu: false, why: "Pas assez d'objets!" };
	    var scTaken={};
	    for (var need = 0; need < needed.length; ++need) {
	        var found = false;
			console.log("needed:"+scene.length);
			console.log(needed[need]);
			
	        for (var nsc = 0; nsc < scene.length; ++nsc) {
	            var obj = scene[nsc];			
				console.log("???"+obj);
	            if (scTaken[nsc]) continue; 
                console.log("compare?");		
	            if (figure.areSame(obj, needed[need])) {
	                found = true;
	                scTaken[nsc] = true;
	                break;
	            }else console.log(obj);
	        }
	        if (!found) return { resu: false, why: JSON.stringify(needed[need]), index:need }
	    }
	    return { resu: true, why: "Tous les objets sont trouvés!" };
	}
};