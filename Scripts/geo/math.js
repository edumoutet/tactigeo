if(typeof(Tactileo)=='undefined') Tactileo={Quiz:{}}
Tactileo.Quiz.geomath=function () {

    var DEGRERAD = Math.PI / 180;

    var EPS = 12;

	function isPoint(p) {
        //return (typeof(p.x) != 'undefined');
        return (p && p.hasOwnProperty('x'));
	}

    /*Pour chercher le point le plus proche de p1: pfound est le candidat retenu provisoire*/
	function getCloser(p1, list) {
	    var pfound = false, dfound = 0;
	    function near(p1, p2) {
	        var d12 = distance(p1, p2);
	        if ((!pfound && d12 < 2 * EPS) || d12 < dfound) { pfound = p2; dfound = d12; }      //p2 remplace pfound
	    }
	    list.forEach(function (p2) { near(p1, p2); });
	    return pfound;
	}
	
    function boxContains(x, y, w, h, pt) {
        return (x <= pt.x && x + w >= pt.x && y <= pt.y && y + h >= pt.y);
    }

    function adaptMILIEU(mil)
    { return milieu(mil.a, mil.b); }

    /****Primitives math�matiques****/
    //Utilitaire pour construction d'arcs.
    function secteur(p1, p2, p3)
    { return [inclinaison(p2, p1), inclinaison(p2, p3)]; }

    function circ_ordre(a, b, c) {
        if (a < c) return (a < b && b < c);
        else return !(c < b && b < a);
    }

    function ordre(a, b, c) {
        return (a <= b && b <= c);
    }

    //Construit [ab] passant par p, de vec. directeur v, centr� en p
    function getVDir(p, v) {
        var p1 = { x: p.x - v.x, y: p.y - v.y };
        var p2 = { x: p.x + v.x, y: p.y + v.y };
        return { a: p1, b: p2 };
    }

    function distance(p1, p2) {
        if (!isNaN(p2)) return p2;   //p2 est un nombre!
        var dx = p1.x - p2.x;
        var dy = p1.y - p2.y;
        var ray2 = dx * dx + dy * dy;
        return Math.sqrt(ray2);  //Pas d'arrondi!
    }

    function egalite(p1, p2)
    { return distance(p1, p2) <= EPS; }

    function estPetit(val) {
        return ordre(-EPS, val, EPS);
    }

    function vecteur(p1, p2) {
        return { x: p2.x - p1.x, y: p2.y - p1.y };
    }

    function orthogonal(p1, p2) {
        return { x: p2.y - p1.y, y: p1.x - p2.x };
    }

    function ptPlusVect(a, v) {
        return { x: a.x + v.x, y: a.y + v.y }
    }

    function norme(v) {
        return Math.sqrt(v.x * v.x + v.y * v.y);
    }

    //Valeurs int�ressantes: -1 pour c sur[p1p2] ,0 pour c sur(p1p2)
    function cosAngle(p1, c, p2) {
        if (egalite(p1, c)||egalite(p2,c)) return -1;  //Il est sur le segment
        var v1 = vecteur(c, p1);
        var v2 = vecteur(c, p2);
        var cosi = v1.x * v2.x + v1.y * v2.y;
        return cosi / norme(v1) / norme(v2);
    }

    //Inclinaison de [p1p2] par rapport axe des abscisses
    function inclinaison(p1, p2) {
        var v = vecteur(p1, p2);
        if (norme(v) < 1) return false;
        var angle = Math.acos(v.x / norme(v));
        if (v.y < 0) angle = -angle;
        return angle;
    }

    /*p in [a,b] ?*/
    function isOnSegment(p, a, b) {
        var x = distToLine(p, a, b);
        if (x < (2 * EPS)) {
            return cosAngle(a, p, b) < 0;
        }
        return false;
    }

    function polaire(angle, rayon) {
        return { x: (rayon * Math.cos(angle)), y: (rayon * Math.sin(angle)) };
    }

    function translate(p, v) {
        return { x: p.x + v.x, y: p.y + v.y };
    }

    /*Ex: al=0, ray=distance(ab) sym�trique de a par rap � b*/
    function avanceTourne(a, b, ray, al) {
        var al = inclinaison(a, b) + al;
        var adv = polaire(al, ray);
        return { x: b.x + adv.x, y: b.y + adv.y };
    }

    /*Image par une rotation suivie d'une homoth�tie (de m�me centre) si ray est fourni (coef homoth�tie=ray/distance(a,b))*/ 
    function imageRota(a, al, b, ray) {
        var al = inclinaison(a, b) + al;
        if (!ray) ray = distance(a, b);
        var adv = polaire(al, ray);
        return {x:a.x+adv.x, y:a.y+adv.y}
    }

    //Utilitaire d'angle: Retourne l'index du point commun pour les 2 segments, false si aucun pt commun.
    function ptcommun(seg1, seg2) {
        if (egalite(seg1.a, seg2.b)) return ['a', 'b'];
        if (egalite(seg1.a, seg2.a)) return ['a', 'a'];
        if (egalite(seg1.b, seg2.b)) return ['b', 'b'];
        if (egalite(seg1.b, seg2.a)) return ['b', 'a'];
        return false;
    }
	
	function ptsEgaux(o1, o2) {
        if (o1.a == o2.a) return (o1.b==o2.b);
        if (o1.b == o2.a) return (o1.a == o2.b);
    }

    //Dit que si on va en sens direct de a1 vers a2, alors l'angle est petit. a1 et a2 dans ]-3.14,3.14]
    function petitDirect(a1, a2)
    { 
		return (a1 > 0 ? (a2 > 0 ? (a1 < a2) : (a1 - a2 > 3.14)) : (a2 < 0 ? (a1 < a2) : (a2 - a1 < 3.14))); 
	}
	
	/*Pour calculer les angles proprement (mesure)
	 * return [x,y] avec x<y et y-x<pi
	 */
	function angleManage(a1,a2){
		var pi=Math.PI;
		if(a1-a2>pi) a2+=(2*pi);
		else if(a2-a1>pi) a1+=(2*pi);
		if(a1<a2) return [a1,a2];
		else return [a2,a1];
	}

    //Donne deux valeurs d'angle...pour d�finir un petit arc!
    //a1,a2 dans ]-pi,pi]
    function reOriente(a1, a2) {
        var bonneOrientation = petitDirect(a1, a2);  
        return (bonneOrientation ? [a1, a2] : [a2, a1]);    
    }

    function orientCircle(circ, pt) {
        var a1 = inclinaison(circ.a, circ.b);   //Besoin cercle avec 2 points.
        var a2 = inclinaison(circ.a, pt);
        circ.orient = (petitDirect(a1, a2) ? 0 : 1);
    }

    //Homoth�rie de centre ctr, rapport rap, renvoie image de M
    function homothetie(ctr, rap, M) {
        var v = vecteur(ctr, M);
        return entier({ x: (ctr.x + rap * v.x), y: (ctr.y + rap * v.y) });
    }

    //Retourne le point N align� sur (CM) avec CN=rayon
    function adaptSegment(ctr, rayon, M) {
        var dist = distance(ctr, M);
        if (dist < 1) return false;
        return homothetie(ctr, rayon / dist, M)
    }

    function milieu(p1, p2) {
        return { x: Math.round((p1.x + p2.x) / 2), y: Math.round((p1.y + p2.y) / 2) };
    }
	
	/*p1+2(p2-p1) pour sym�trie par rapport � p2*/
	function symetrieCentre(p1,p2){
		return homothetie(p2,-1,p1);
	}
	
	function symetrieAxe(p1,axe){
		var p2=projeter(p1,axe);
		return homothetie(p2,-1,p1);
	}

    function entier(pt)
    { return { x: Math.round(pt.x), y: Math.round(pt.y) }; }

    //Retourne: rien
    function arrondit(pt, grid) {
        if (!grid) return pt;
        var dx = pt.x % grid;
        if (dx > grid / 2) dx -= grid;  //Revient � augmenter x de grid, donc on a bien un arrondi.
        pt.x -= dx;
        var dy = pt.y % grid;
        if (dy > grid / 2) dy -= grid;
        pt.y -= dy;
    }

    /**return point|false**/
    function intersection(seg1, seg2) {   
        var coefs=coefInter(seg1,seg2);
		if(det=coefs[0])
           return homothetie(seg1.a, coefs[1]/det, seg1.b);
	    return false;
    }
	
	/**(AB)int(CD) donne AM = la AB et CM = mu CD**/
	function coefInter(seg1,seg2){
		var v1 = vecteur(seg1.a, seg1.b);
        var v2 = vecteur(seg2.b, seg2.a);
        var v3 = vecteur(seg1.a, seg2.a);

        var det = v1.x * v2.y - v1.y * v2.x;
        var la = v3.x * v2.y - v3.y * v2.x;
		var mu = -v3.x * v1.y + v3.y * v1.x;
		//console.log(la/det);
		//console.log(mu/det);
        return [det,la,mu];
	}
	
	/**Coord barycentriques de M ds triangle (A,B,C)**/
	function coefBary(A,B,C,M){
		var seg1={a:A,b:M}
		var seg2={a:B,b:C}
		var data=coefInter(seg1,seg2);
		if(det=data[0]) {
			la=data[1]/det;
			mu=data[2]/det;
			return [la-1,1-mu,mu];
		}
	}

    /**segment 2 donn� par pt+direction**/
    function intersection2(seg1, B, dirb) {
        var v1 = vecteur(seg1.a, seg1.b);
        var v2 = dirb;
        var v3 = vecteur(seg1.a, B);
        var det = v1.x * v2.y - v1.y * v2.x;

        if (!det) return false;
        var lambda = (v3.x * v2.y - v3.y * v2.x) / det;

        return homothetie(seg1.a, lambda, seg1.b);
    }

	/*Intersection de droite avec cercle bas� sur les deux points*/
	function symCentrale(seg,cer){   //Oblig�: ptsEgaux(seg,cer)
		return homothetie(cer.a,-1,cer.b);
	}
	
    function segmentCutCircle(seg, cer) {
        var cn = cer.getCenter(), cosa;
        if (egalite(seg.a, cn) || egalite(seg.b, cn)) cosa = 1;
        else cosa = cosAngle(seg.b, seg.a, cn);  //Angle BAN avec N: centre du cercle
        var AO = distance(seg.a, cn);
        var AH = AO * Math.abs(cosa);  //H: projet� du centre du cercle sur la droite.
        var R = cer.getRayon(), r;
        var r2 = R * R - AO * AO * (1 - cosa * cosa);  //Distance du projet� vers l'un des deux points
        //console.log("segmentCutCircle:");
        //console.log(R + '|' + AO + '|' + cosa+'|'+r2);
        if (r2 > 0) {
            r = Math.sqrt(r2);
            seg_mes = distance(seg.a, seg.b);
            var coefH1 = (AH - r) / seg_mes, coefH2 = (AH + r) / seg_mes;
            var pt1 = homothetie(seg.a, coefH1, seg.b);
            var pt2 = homothetie(seg.a, coefH2, seg.b);
            if (r < EPS / 2) pt2 = pt1;
            return [pt1, pt2];
        }
        else return false;
    }

    function segmentCutArc(seg, arc) {
        var pair = segmentCutCircle(seg, arc);      
        if (!pair) return false;
        for (var np = 0; np < 2; ++np) {
            var pt = pair[np];
            var c1 = seg.selec(pt);
            var c2 = arc.selec(pt);     
            if (seg.selec(pt) && arc.selec(pt)) return pt;
        }
        //console.log("segmentCutArc:Echec");
    }

    function circleCutArc(circ, arc) {
        var pair = interCircle(circ, arc);
        if (!pair) return false;
        for (var np = 0; np < 2; ++np) {
            var pt = pair[np];
            if (arc.selec(pt)) return pt;
        }     
    }

    function arcCutArc(arc1, arc2) {
        var pair = interCircle(arc1, arc2);
        if (!pair) return false;
        for (var np = 0; np < 2; ++np) {
            var pt = pair[np];
            if (arc2.selec(pt)&&arc1.selec(pt)) return pt;
        }
    }

    function interCircle(c1, c2) {
        var r1 = c1.getRayon(), r2 = c2.getRayon();
        var cn1 = c1.getCenter(), cn2 = c2.getCenter();
        var dray = r1 * r1 - r2 * r2;
        var dcentre = distance(cn1,cn2);
        var incliCentres = inclinaison(cn1,cn2);

        var x = (dray / dcentre + dcentre) / 2;
        if (r1 < x) return false;

        else if (r1 == x) {
            var v1 = polaire(incliCentres, r1);
            return { x: cn1.x + v1.x, y: cn1.y + v1.y }
        }
        var h = Math.sqrt(r1 * r1 - x * x);       
        var incliSolu = Math.acos(x/r1);
        var v1 = polaire(incliCentres + incliSolu, r1);
        var v2 = polaire(incliCentres - incliSolu, r1);
        return [{ x: cn1.x + v1.x, y: cn1.y + v1.y }, { x: cn1.x + v2.x, y: cn1.y + v2.y }];
    }

    /*Retourne les zeros, exp�rimental: on a besoin de franchir 0*/
    function findZero(meth, interval) {
        //var interval = findInterval(0, repere.x, g.wid);
        var pas = .1;
        var mini = interval[0];
        var result = [];
        var sig = sign(-meth(mini));  //Sig in [-1;0;1]
        for (var x = mini + pas; x <= interval[1]; x += pas) {
            var y = -meth(x);
            var sig2 = sign(y);
            if (y == 0) result.push(x);
            else if (sig && sig != sig2) result.push(x - pas / 2);   //todo: franchissement de 0?
            sig = sig2;
        }
        return result;
    }

    function sign(x) { return (x < 0 ? -1 : (x == 0 ? 0 : 1));}

    /*Surface d'un triangle*/
    function surface(a, b, c) {
        var d1 = distance(a, b);
        var d2 = distance(b, c);
        var d3 = distance(c, a);
        var p = (d1 + d2 + d3) / 2;
        return Math.sqrt(p * (p - d1) * (p - d2) * (p - d3));  //Heron!!
    }

    /**Projet� de pt sur seg*/
    function projeter(pt, seg) {
        var ortho = orthogonal(seg.a, seg.b);
        return intersection2(seg, pt, ortho);
    }

    /*resu: M avec AM = dist et M sur (AB)*/
    function onSegment(seg, dist) {
        var mes_seg = distance(seg.a, seg.b);
        return homothetie(seg.a, dist / mes_seg, seg.b);
    }

    /**Distance de a � [bc] - pb de pr�cision**/
    function distToLine_(a, b, c) {
        var s = 2 * surface(a, b, c);
        return s / distance(b, c);  //Longueur de la hauteur issue de a dans le triangle abc: a*dist=2s
    }

    function distToLine(a, b, c) {
        var ortho = orthogonal(b, c);
        var proj = intersection2({ a: b, b: c }, a, ortho);
        return distance(a, proj);
    }

    function surface_test() {
        return surface({ x: 10, y: 50 }, { x: 10, y: 100 }, { x: 100, y: 50 });
    }

    function distToLineTest() {
        return distToLine({ x: 10, y: 50 }, { x: 10, y: 100 }, { x: 100, y: 50 });
    }

    function gravityCenter(tab) {
        var x = tab[0].x;
        var y = tab[0].y;
        for (var nd = 1; nd < tab.length; ++nd) {
            x += tab[nd].x;
            y += tab[nd].y;
        }
        return { x: x / tab.length, y: y / tab.length }
    }

    function polyDistance(poly, p) {
        var dist = distance(poly[0], p);
        for (var nd = 0; nd < poly.length; ++nd) {
            dist += distance(poly[nd], p);
        }
        return dist;
    }

    /**pt est-il sur arc ax� sur [cnter,ptArc]?*/
    function isOnSmallArc(cnter,ptArc,pt){
        var incli = inclinaison(cnter, ptArc);
        var dray = distance(cnter, ptArc)-distance(cnter,pt);
        return estPetit(dray) && ordre(incli - .5, inclinaison(cnter, pt), incli + .5);
    }

    math = {
        call: function (name) { return eval(name); },EPS: EPS, getCloser: getCloser,
        vecteur: vecteur, orthogonal: orthogonal, inclinaison: inclinaison,cosAngle: cosAngle, projeter: projeter, distToLine: distToLine, isOnSegment: isOnSegment,
        polaire: polaire, ptPlusVect: ptPlusVect, milieu: milieu, avanceTourne: avanceTourne,
        intersection: intersection, symCentrale: symCentrale, angleManage: angleManage,
		boxContains: boxContains, ptcommun: ptcommun, ptsEgaux: ptsEgaux,
		translate: translate, coefBary: coefBary, symAxe: symetrieAxe,
        segmentCutCircle: segmentCutCircle, segmentCutArc: segmentCutArc, interCircle: interCircle,
		circleCutArc: circleCutArc, arcCutArc: arcCutArc,
        reOriente: reOriente,petitDirect: petitDirect, circ_ordre: circ_ordre, ordre: ordre, estPetit: estPetit,
        homothetie: homothetie, imageRota: imageRota, isOnSmallArc: isOnSmallArc,
        egalite: egalite, entier: entier, distance: distance, arrondit: arrondit, findZero: findZero,
        gravityCenter: gravityCenter, polyDistance: polyDistance, isPoint: isPoint
    }
};