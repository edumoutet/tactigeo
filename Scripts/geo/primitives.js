﻿Tactileo.Quiz.prim=function() {

    var COLONNE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    var RED = "#ff8899";
    var GREEN = "#88ff99";
    var BLUE = "#8899ff";
    var SELCOLOR = '#ee8899';
    var NOSELCOLOR = '#333333';
    var LIGHTCOLOR = "#aaccee";
    var CRAYON = "#b0b0b0"; 

    /*Pattern déprécié pour hachure */
    var PATTERN = false;
    var colors = [CRAYON, 'orange', 'red', 'green', 'blue', 'black', LIGHTCOLOR, 'yellow'];
    var hexcolor = ['#9e9e9e', '#eeee88', '#ff0000', '00ff00', '#0000ff', '#111111', "#aaccee", '#99ee44'];

    var EPS = math.EPS;

    function doc(id) { return document.getElementById(id); }

    /*Utilitaires de positionnement*/
    function setCoord(point) {
        var realCoord = prim.gfxToCoord(point.x, point.y);
        var appCoord = appr(realCoord, 10);
        return '(' + appCoord.x + ';' + appCoord.y + ')';
    }

    function appr(point, deci) {
        var decix = deci / plane.repere.unitx;  //Ex: unitx=0.1=>100 ie 2 décimales
        var deciy = deci / plane.repere.unity;
        return { x: Math.round(point.x * decix) / decix, y: Math.round(point.y * deciy) / deciy };
    }

    function frmatDecimal(nmber, deci) {
        return Math.round(nmber * deci) / deci;
    }

    /**intervalle des x pour la fonction: gfx=>real**/
    function findInterval(x1, x, x2) {
        var ratio = plane.repere.unitx / plane.gridInfo.gridx/2;
        var xmin = ratio * (x1 - x) , xmax = ratio* (x2 - x);
        return [xmin, xmax];
    }

    /*Utilitaires de dessin*/

    /*Remplissage de formes*/
    function initPattern(path) {
        var img = new Image(48, 48);
        img.src = path;
        prim.pattern = function (ctx) { return ctx.createPattern(img, "repeat") };
    }

    /**Appel avec plane.redraw*/
    function setDrawParams(fat) {
        prim.lineWidth = fat ? 8 : 2;
        prim.selWidth = fat ? 8 : 2;
        prim.font = fat ? '20pt Calibri' : '12pt Calibri';
        prim.rayPoint = fat ? EPS : EPS / 3;
    }

   
    function translate_color(color) {
        var nd = colors.indexOf(color);
        if (nd >= 0) return hexcolor[nd];
        return prim.CRAYON;
    }
    
    function setColor(color)
    { plane.ctx.strokeStyle = color; }

    /*Si objet rempli, sa bordure est peinte en gris*/
    function solve_color(obj,ctx,sel) {
        var color = obj.color;
        ctx.strokeStyle = "gray";
        ctx.fillStyle = "gray";
        if (sel) //{color = prim.SELCOLOR; ctx.strokeStyle = color; }//Impose bordure en sélection!
        {
            ctx.setLineDash([15, 15]);
            ctx.strokeStyle = prim.SELCOLOR;
        }
        else if (!obj.fill) {
            if (color == "pattern") ctx.setLineDash([5, 15]);
            else if (color) ctx.strokeStyle = color;
        }
        else if (color == "pattern") ctx.fillStyle = prim.pattern(ctx);
        else ctx.fillStyle = color;
    }

    /*Ancien...*/
    function beginDraw(color,width) {
        var ctx = plane.ctx;
        if (width) ctx.lineWidth = width;
        else if (color == SELCOLOR) ctx.lineWidth = prim.selWidth;
        else ctx.lineWidth = prim.lineWidth;
        if (color == "pattern") ctx.setLineDash([5, 15]);  //PATTERN(ctx);=>Pointillés
        else if (color != "keep") ctx.strokeStyle = (color ? color : NOSELCOLOR);
        ctx.beginPath();
        return ctx;
    }

    function completeDraw(obj, surligne)
    {
        var ctx = plane.ctx;
        solve_color(obj, ctx, surligne);
        ctx.beginPath();
        obj.rely(ctx);
        ctx.closePath();
        ctx.setLineDash([]);
        ctx.fillStyle = null;
    }

    //Dessine un point. col: couleur; ndAnno: index alphabet
    function draw(point, surligne, ndAnno) {
		var context = plane.ctx;
		   
        /*Emplacement*/
		context.beginPath();
		context.lineWidth = prim.lineWidth;
		context.strokeStyle = "gray";
        context.arc(point.x, point.y, prim.rayPoint, 0, 6.28);  //point=cercle de petit rayon
        context.stroke();

        /*Texte*/
		var txt=point.text;
		if(point.coord) {
		    txt = setCoord(point);
		}
        if (!txt && (ndAnno + 1)) {
            txt = (isNaN(ndAnno) ? ndAnno : COLONNE.charAt(ndAnno));
            prim.indexPoint[txt] = point;
		}
        if (txt) {
            context.lineWidth = 1;
            context.strokeText(txt, point.x - EPS, point.y - EPS);  //NB: n'impose pas de couleur
            context.lineWidth = prim.lineWidth;
        }
        
        /*Remplir si particularité*/
        if (surligne) {
            context.fillStyle = prim.SELCOLOR;  
            context.fill();
        }
        else if (point.discret) { }//Aucun remplissage
        else if (point.update) {
            context.fillStyle = "blue";  //Point dépendant.
            context.fill();
        }
        context.closePath();
    }

    

    function fillPoint(ctx, point, color) {
        ctx.beginPath();
        ctx.fillStyle = color;
        ctx.arc(point.x, point.y, prim.rayPoint, 0, 6.28);  //point=cercle de petit rayon
        ctx.fill();
        ctx.closePath();
    }

    function drawGrid(dogrid) {
        var context = plane.ctx;
        context.globalAlpha = 1.0;
        with (plane.gridInfo) {//wid,hei,gridx,gridy
            context.fillStyle = "#ffffff";
            context.fillRect(0, 0, wid, hei);
            if (!dogrid) return;
            context.beginPath();
            context.strokeStyle = "#aaeeaa";
            context.lineWidth = prim.lineWidth;
            var nbptx = wid / gridx, nbpty = hei / gridy;
            for (col = 0; col <= nbptx; ++col)//>colonnes
            {
                context.moveTo(gridx * col, 0);
                context.lineTo(gridx * col, nbpty * gridy);
            }
            for (lig = 0; lig <= nbpty; ++lig)//>lignes
            {
                context.moveTo(0, gridy * lig);
                context.lineTo(nbptx * gridx, gridy * lig);
            }
        }
        context.closePath();
        context.stroke();
    }
	
	/*règle en svg*/
	function drawRuleSvg(xmax){
		var result="<g style=\"fill:none;stroke-width:1px;stroke:blue\">";
		for(var xcur=0;xcur<xmax;xcur+=plane.gridInfo.gridx)
			result+="<line x1="+xcur+" y1=0 " + "x2="+xcur+" y2=30></line>";
		return result + "</g>";
	}
	
	function drawRule(can,xmax){
		var ctx=can.getContext("2d");
		ctx.strokeStyle="blue";
		ctx.moveTo(0,15);
		ctx.lineTo(xmax,15);
		ctx.stroke();
		var pas=plane.gridInfo.gridx;
		var coord=0;
		for(var x=0;x<xmax;x+=pas){
			ctx.moveTo(x,0);
			ctx.lineTo(x,30);
			ctx.stroke();
			if(!(coord%2)) ctx.strokeText(coord/2,x+2,15);
			++coord;
		}
	}

    function writeMessageCanvas(message, x, y, font) {
        var context = plane.ctx;
        context.font = font ? font : prim.font;
        context.fillStyle = 'blue';
        context.fillText(message, x, y);
    }

    /***SEGMENT***/
    function relySEGMENT(p1, p2, color) {
        //var context = beginDraw(color);
        if(color) plane.ctx.strokeStyle = color;
        straight(p1, p2, plane.ctx);
    }

    function straight(p1, p2, context) {
        //context.globalCompositeOperation="xor";
        context.moveTo(p1.x, p1.y);
        context.lineTo(p2.x, p2.y);
        context.stroke();
    }

    /**Sinusoide entre p1 et p2*/
    function sine(p1, p2, context, pas) {
        context.moveTo(p1.x, p1.y);
        if (p1.x == p2.x || p1.y == p2.y) context.lineTo(p2.x, p2.y);  //Ligne simple.
        else {
            var m = math.milieu(p1, p2);
            var diff = math.vecteur(p1, p2);
			var a = p1.x, b = p2.x;
            if (a > b) { b = p1.x; a = p2.x; context.moveTo(p2.x, p2.y);}  //Requis: a<b
            for (var x = a; x<b; x += pas) {      
                context.lineTo(x, m.y + diff.y / 2 * Math.sin(Math.PI * (x - m.x) / diff.x));
            }
            
        }
        context.stroke();
    }

    //Depuis le segment [ab], avance de ray avec un angle al par rapport à [ab].
    function avanceTourne(a, b, ray, al) {
        var pt = math.avanceTourne(a, b, ray, al);
        relySEGMENT(b, {x:Math.round(pt.x),y:Math.round(pt.y)});
    }

    //Flèche au bout de [p1p2]
    function arrow(p1, p2) {
        var al = 3 * Math.PI / 4;
        avanceTourne(p1, p2, 20, al);
        avanceTourne(p1, p2, 20, -al);
    }

    function compas(a, b) {
        var d = math.distance(a, b);
        var pt = math.avanceTourne(a, b, d * .7, 2.5);
        relySEGMENT(a, pt, prim.LIGHTCOLOR);
        relySEGMENT(b, pt, prim.LIGHTCOLOR);
    }

    /**Annote un segment*/
    function annoteSegment(a, b, note) {
        with (math) {
            var v = orthogonal(a, b);
            v.x = v.x / 8;
            v.y = v.y / 8;
            //relySEGMENT(a, b);
            var p1 = ptPlusVect(a, v);
            var p2 = ptPlusVect(b, v);
            var m = milieu(p1, p2);
        }
        prim.writeMessage(note, m.x, m.y, 'italic 12pt Calibri');
        /*arrow(p1,p2);*/
        return { note: note, position: m };
    }

    /*Pour annoter un segment!*/
    function demiEllipse(a, b, ctx) {
        var incli = math.inclinaison(a, b);
        var mil = math.milieu(a, b);
        var diam = math.distance(a, b);
        ctx.beginPath();
        ctx.strokeStyle = "#aaa";
        ctx.setLineDash([5, 15]);
        ctx.ellipse(mil.x, mil.y, diam / 2, diam / 8, incli, 3.14, 6.28);
        ctx.stroke();
    }

    function etoile(x, y, size) {
        var smoothColor = "#aa9966";
        relySEGMENT({ x: x - size, y: y + size }, { x: x + size, y: y - size }, smoothColor);
        relySEGMENT({ x: x - 10, y: y - 10 }, { x: x + 10, y: y + 10 }, smoothColor);
    }

    /**tab: tableau de points à translater.**/
    function translater(tab, baseTrans, p) {
        var v = vecteur(baseTrans, p);
        for (ndex in tab) {
            with (tab[ndex]) {
                x = Math.round(x) + v.x;
                y = Math.round(y) + v.y;
            }
        }
        redraw();
    }

    //Appelé après creeObjet et selecObjet(succes)
    function selecShow(obj, text, color) {
        if (obj.name == "Message") return;  //todo: gérer ce cas
        if (!color) color = SELCOLOR;
        var posText;
        if (!math.isPoint(obj)) { completeDraw(obj,color); posText = obj.a; }
        else { draw(obj, color); posText = obj; }

        if (text) {
            prim.writeMessage(text, posText.x, posText.y);
        }
    }

    function relyArc(ctx, a, p) {
        ctx.strokeStyle = prim.LIGHTCOLOR;
        with (math) {
            var rayon = distance(a, p);
            var incli = inclinaison(a, p);
            var alpha = reOriente(incli - .5, incli + .5);  //Environ 60°
        }
        ctx.arc(a.x, a.y, Math.round(rayon), alpha[0], alpha[1]);
        ctx.stroke();
    }


    /**Repère-fonctions
    a: abscisse la + petite
	len: graduation la + à droite
	delta:écart égal à l'unité
	**/
    function relyScaleX(a, len, delta, ctx) {
        var minx = Math.round(-a.x / delta);  //Coordonnée la plus petite, la coordonnée est affichée.
        ctx.strokeStyle = "#89e";
        //var write = (ctx == svgContext ? ctx.writeMessage : prim.writeMessage);
        for (var npos = 0; npos < len; npos += delta) {
            var pt1 = { x: npos, y: a.y - 4 }
            var pt2 = { x: npos, y: a.y + 4 }
            prim.straight(pt1, pt2, ctx);
            if (minx) prim.writeMessage("" + frmatDecimal(minx*plane.repere.unitx,10), pt1.x, pt1.y);
            ++minx;
        }
    }

    function relyScaleY(a, len, delta,ctx) {
        var miny = Math.round(-a.y / delta);
        ctx.strokeStyle = "#89e";
        for (var npos = 0; npos < len; npos += delta) {
            var pt1 = { x: a.x - 12, y: npos }
            var pt2 = { x: a.x + 12, y: npos }
            prim.straight(pt1, pt2, ctx);
            if (miny) prim.writeMessage("" + frmatDecimal(-miny*plane.repere.unity,10), pt1.x, pt1.y);  //Attention numérotation de bas en haut!
            ++miny;
        }
    }

    function relyFunction(meth, ctx) {
        var repere = plane.repere;
        var g = plane.gridInfo;
       
        var interval = findInterval(0, repere.x, g.wid);  //Traduire pixel canvas en coordonnées. 
        var mini=interval[0];
        var pas = 4 * repere.unitx / g.gridx;
        var dep = prim.coordToGfx (mini, -meth(mini) );  //-meth à cause canvas de bas en haut!
        
        ctx.moveTo(dep.x, dep.y);  //Pt le plus à gauche
        for (var x = mini + pas; x <= interval[1]; x += pas) {
            var point = prim.coordToGfx(x, -meth(x));
            ctx.lineTo(point.x, point.y);
        }
        ctx.stroke();
    }

    function relyPolar(meth, ctx) {
        var repere = plane.repere;
        var g = plane.gridInfo;
        var pt0 = math.polaire(0, meth(0));
        pt0 = prim.coordToGfx(pt0);
        //ctx.beginPath();
        ctx.moveTo(pt0.x, pt0.y);
        for (var ang = .1; ang < 6.28; ang += .1) {
            var ro = meth(ang);
            var pt = math.polaire(ang, ro);
            pt=prim.coordToGfx(pt.x,pt.y);
            ctx.lineTo(pt.x, pt.y);
        }
        //ctx.closePath();
        ctx.stroke();
    }
	
	function relyPolygon(tab,ctx,close){
		if(tab.length<2) return;
		var pt0 = tab[0];
		ctx.beginPath();
        ctx.moveTo(pt0.x, pt0.y);
        for (var ndpt in tab) {
            ctx.lineTo(tab[ndpt].x, tab[ndpt].y);
        }
        if (close) {
            ctx.lineTo(pt0.x, pt0.y);
            ctx.closePath();
        }
        ctx.closePath();
		ctx.stroke();
	}
	
	function relyRound(tab,ctx,close){
		if(tab.length<2) return;
		var pt0 = tab[0];
		ctx.beginPath();
        ctx.moveTo(pt0.x, pt0.y);
		var pnext=tab[1];
        for (var nd=0;nd<tab.length-1;++nd) {
			var pnext=tab[nd+1];
            ctx.arcTo(tab[nd].x, tab[nd].y, pnext.x, pnext.y,8);
        }
        ctx.lineTo(pnext.x, pnext.y);
        ctx.closePath();
        /*if(close) {ctx.lineTo(pt0.x, pt0.y);
                   ctx.closePath();
		}*/
		ctx.stroke();
	}

    function selecFunction(meth, pt) {
        var repere = plane.repere;
        var x = (pt.x - repere.x) / plane.scale.dispx;  //Unité=2*gridx
        var y = (pt.y - repere.y) / plane.scale.dispy;
        return (Math.abs(y + meth(x)) < .25);
    }
	
    prim = {
        degres:function(rads){return Math.round(180*rads/Math.PI);},
        initPattern: initPattern, colors: function () { return colors }, hexcolor:hexcolor, translate_color: translate_color,
        call: function (name) { return eval(name); }, CRAYON: CRAYON, LIGHTCOLOR: LIGHTCOLOR, SELCOLOR: SELCOLOR, BLUE: BLUE, COLONNE: COLONNE,
        setDrawParams: setDrawParams, compas: compas, demiEllipse: demiEllipse,
        drawGrid: drawGrid, drawRule: drawRule, selecShow: selecShow, draw: draw, writeMessage: writeMessageCanvas, arrow: arrow, fillPoint:fillPoint,
        relySEGMENT: relySEGMENT, relySine: sine, relyArc: relyArc, relyPolygon: relyPolygon, relyRound: relyRound,
        relyScaleX: relyScaleX, relyScaleY: relyScaleY, relyFunction: relyFunction,selecFunction: selecFunction, relyPolar: relyPolar,
        beginDraw: beginDraw, completeDraw: completeDraw, annoteSegment: annoteSegment, straight: straight, findInterval: findInterval,
        charAtIndex: function (nd) { return COLONNE[nd]; }, indexPoint: {}
    }

    /*Gestion de coordonnées*/
    prim.coordToGfx = function (x, y) {
        with (plane) return { x: Math.round(repere.x + x * scale.dispx), y: Math.round(repere.y + y * scale.dispy) };
    }

    prim.gfxToCoord = function (x, y) {
        with (plane) {
            return { x: (x - repere.x) / scale.dispx, y: (repere.y - y) / scale.dispy };
        }
    }

    prim.distance = function (a, b) {
        var a1 = prim.gfxToCoord(a.x, a.y);
        var a2 = prim.gfxToCoord(b.x, b.y);
        return math.distance(a1, a2);
    }

};






 

