﻿/*Pour faire des appels sous forme de script et non pas via IHM*/
var scripter;
(function () {
    /*Rechercher une figure par identifiants points
     * SELEC AB
     */
    function selecLine(a, b) {
        var objs = plane.objets();
        var pta = prim.indexPoint[a];
        var ptb = prim.indexPoint[b];
        if (!pta || !ptb) return { ok: false, message: "Points unreachable" }
        for(var nobj=0;nobj<objs.length;++nobj)
        { var obj = objs[nobj];
            if ((obj.a == pta && obj.b == ptb) || (obj.a == ptb && obj.b == pta)) {
                plane.Selected(obj);
                return { ok: true, message: obj }
            }
        }
        return { ok: false, message: "Object not found" }
    }

    /*line=segment ou droite
     * POINT AB .75
     */
    function ptOnLine(a, b, coef) {
        var line = selecLine(a, b);
        if (!line.ok) return;
        line = line.message;
        var newpt = math.milieu(line.a, line.b);
        if (coef) newpt = math.homothetie(line.a, coef, line.b);
        plane.addGlueOn(newpt, line);
        plane.redraw();
    }

    /*Pour intersection gérée par geocontrol (droite,cercle)
     * Intersection AB CD 
     */
    function interLine(a,b,c,d) {
        var s1 = selecLine(a,b);
        var s2 = selecLine(c,d);
        if (s1.ok && s2.ok) {
            console.log("interline=>try intersect");
            geocontrol.tryIntersect2(s1.message, s2.message);
            plane.redraw();
        }
    }

    /*Relie les points par un segment ou cercle de centre a passant par b
     * CERCLE A B
     */
    function creeFigure(a, b, nom) {
        var pta = prim.indexPoint[a];
        var ptb = prim.indexPoint[b];
        if (pta && ptb) plane.addObject(nom, pta, ptb);
        plane.redraw();
    }

    function sector(a, b, ray, al) {
        var pta = prim.indexPoint[a];
        var ptb = prim.indexPoint[b];
        var cer = plane.addObject("SEGMENT", pta, ptb);
        cer.hidden = true;
        console.log(cer);
        var ptc = math.imageRota(pta, al, ptb);
        console.log(ptc);
        plane.add(ptc);
        plane.addObject("SECTOR", cer, ptc);
        plane.redraw();
    }

    /**Bloc de commande avec repeter possible - sous-commandes possibles*/
    function TURTLE(first) {
        var turtle = this;
        turtle.link = [];
        turtle.angle = 0;
        this.goto = function (pt, a) { this.start = pt; this.angle = a; }
        this.nbiter = 1;
        this.dyna = function (r, a)
        { turtle.link.push({ r: r, a: a }); }  //Ajout commande dynamique: avance, tourne

        this.rely = function (ctx) {
            var x = this.start.x;
            var y = this.start.y;
            var angle = turtle.angle;

            if (first) ctx.moveTo(this.start.x, this.start.y);
            for (var iter = 0; iter < turtle.nbiter; ++iter) {
                turtle.link.forEach(function (dyn) {
                    if (dyn instanceof TURTLE)//Possibilité d'imbriquer
                    {
                        dyn.goto({ x: x, y: y }, angle);
                        var res = dyn.rely(ctx); x = res.x; y = res.y; angle = res.a; return;
                    }
                    angle += dyn.a;
                    x = x + dyn.r * Math.cos(angle);
                    y = y + dyn.r * Math.sin(angle);
                    ctx.lineTo(x, y);
                });
            }
            ctx.stroke();
            return { x: x, y: y, a: angle }
        }
        this.selec = function () { return false; }
    }

    /**script turtle:
    Params : nbiter, angle
     * moveto 200,300
     * repeter nbiter
     *   avance 100 
     *   tourne angle
     *   repeter 5
     *     avance 50
     *     tourne 36
     *   fin
     * fin
     */
    function exTurtle(nbiter, angle) {
        var t = new TURTLE(true);
        t.nbiter = nbiter;
        t.goto({ x: 200, y: 300 }, 0);
        t.dyna(100, angle);
        var t1 = new TURTLE();
        t1.nbiter = 5;
        t1.dyna(50, Math.PI / 5);
        t.link.push(t1);
        plane.objets.push(t);
    }

    scripter = { selecLine: selecLine, ptOnLine: ptOnLine, interLine: interLine, creeFigure: creeFigure, sector: sector, exTurtle: exTurtle }

})();