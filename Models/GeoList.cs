﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GeoLab.Models
{
    public class GeoList
    {
        public IEnumerable<GeoModel> geoModels;
        public MongoCollection<GeoModel> mongoModels;
        public GeoModel selectedModel;
        public GeoList()
        {
            mongoModels = MongoHelper.Instance.Database.GetCollection<GeoModel>("geomodel");
            geoModels = new GeoModel[]{new GeoModel()}.Union(mongoModels.FindAll());  //Premier modèle: vide!
            selectedModel=this.getOne();
            if (selectedModel == null) selectedModel = new GeoModel();
        }

        public GeoModel getOne()
        {
            return mongoModels.FindOne(Query<GeoModel>.Where(x=>x.title=="Cercle"));
        }
    }
}