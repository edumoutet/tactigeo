﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Web.Script.Serialization;

namespace GeoLab.Models
{
  
    public class GeoModel
    {
       public ObjectId _id{get; set;}
       public string title{get;set;}
       public string description;
       public List<GeoObject> liste=new List<GeoObject>();
       public Option figure_option = new Option();

       public string serialize()
       {
           var jsonSerialiser = new JavaScriptSerializer();
           var json = jsonSerialiser.Serialize(this);
           return json;
           /**StreamWriter file = new System.IO.StreamWriter("test.geo");
           file.WriteLine(json);
           file.Close();**/
       }

       public GeoModel()
       {
           //this.Id = new Guid();
           title = "GeoModel vide";
           liste.Add(GeoObject.geoRepere);
           //BsonSerializer.LookupSerializer(typeof(GeoObject));
           //BsonSerializer.LookupSerializer(typeof(GeoFigure));
       }

        /*Données venant d'1 requête web en principe*/
       public GeoModel(string sdata, string stitle, string soption, string sid)
       {
           var jsonSerialiser = new JavaScriptSerializer();
           //BsonClassMap.RegisterClassMap<GeoFigure>();
           
           this.liste = (List<GeoObject>)(jsonSerialiser.Deserialize(sdata, typeof(List<GeoObject>)));
           this.figure_option = (Option)(jsonSerialiser.Deserialize(soption, typeof(Option)));
           this.title = stitle;
           if(sid != null) this._id = ObjectId.Parse(sid);
       }
  
    }

    [BsonDiscriminator(RootClass = true)]
    [KnownType(typeof(GeoFigure))]
    [BsonKnownTypes(typeof(GeoFigure))]
    public class GeoObject
    {
        public string name; 
        public Creator creator;
        public int[] link;
        
        public Point point;
        public bool hidden;

        //public Dictionary<string, string> duos;
        public Pair[] property;

        public class Creator
        {
            public int[] link;
            public string name;
        }

        public class Point { public int x, y; public bool locked;}

        public class Pair { public string key, value;}

        public static Pair addProp(string key, string value) { return new Pair() { key = key, value = value }; }

        public static GeoObject geoRepere = new GeoObject() { name = "REPERE", 
            hidden = true, 
            point =new Point() {x=200,y=100 },
            property=new Pair[]{addProp("text","O")}};

        public override string ToString()
        {
            var jsonSerialiser = new JavaScriptSerializer();
            return jsonSerialiser.Serialize(this);
        }
    }

    /*Options globales à la figure*/
    public class Option
    {
        public bool AB;
        public bool gridset;
    }

    /**[BsonDiscriminator("geo_figure")]**/
    public class GeoFigure : GeoObject
    {
        public int dummy = 1;         
    }

}