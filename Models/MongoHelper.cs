﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MongoDB.Bson;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace GeoLab.Models
{
    public class MongoHelper
    {
        #region Singleton

        private static readonly MongoHelper _instance = new MongoHelper();

        private MongoHelper()
        {
            // MongoDB deserializer is set to ignore extra elements on
            // types from Tactileo.Webservices namespace
            var conventions = new ConventionPack();
            conventions.Add(new IgnoreExtraElementsConvention(true));
            ConventionRegistry.Register(
                "Tactileo Conventions",
                conventions,
                (type) => type.FullName.StartsWith("Tactileo.Webservices.")
            );

            this.Client = new MongoClient(
                //Config.AppSettings.GetConfigValue(Config.AppSettings.Keys.MongoConnectionString)
            );
            this.Server = this.Client.GetServer();
            this.Database = this.Server.GetDatabase("geobook"
                //Config.AppSettings.GetConfigValue(Config.AppSettings.Keys.MongoDatabaseName)
            );
        }

        public static MongoHelper Instance
        {
            get { return _instance; }
        }

        #endregion

        public MongoClient Client { get; private set; }
        public MongoServer Server { get; private set; }
        public MongoDatabase Database { get; private set; }
    }

    public class MongoHelper<T> where T : class
    {
        #region Singleton

        private static readonly MongoHelper<T> _instance = new MongoHelper<T>();

        private MongoHelper()
        {
            this.Collection = MongoHelper.Instance.Database.GetCollection<T>(typeof(T).Name.ToLower());
        }

        public static MongoHelper<T> Instance
        {
            get { return _instance; }
        }

        #endregion
        
        public MongoCollection<T> Collection { get; private set; }

        public T GetItemById(ObjectId id)
        {
            return this.Collection.FindOneById(id);
        }

        public ObjectId SaveItem(T item, bool exist)
        {
            var bson = new BsonDocument(item.ToBsonDocument());
            if (!exist)
            {
                WriteConcernResult bidule=
                this.Collection.Save(bson);
                //var aaa = bidule.ToBsonDocument();
            }
            else UpdateItem((ObjectId)bson["_id"], item);
            var id = bson["_id"];
            return (ObjectId)id;
        }

        public void UpdateItem(ObjectId id, T item)
        {
            var update = new UpdateDocument(item.ToBsonDocument());
            this.Collection.Update(Query.EQ("_id", id), update);
        }

        public void UpdateDate(Guid id, string fieldname, DateTime date)
        {
            var args = new FindAndModifyArgs()
            {
                Query = Query.EQ("_id", id),
                SortBy = SortBy.Null,
                Update = Update.Set(fieldname, date)
            };
            this.Collection.FindAndModify(args);
        }

        public void DeleteById(ObjectId id)
        {
            this.Collection.Remove(Query.EQ("_id", id));
        }


    }
}
  